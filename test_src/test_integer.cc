#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_integer)
	using namespace h119::syntx;

	std::string test = "-123";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("integer");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= integer();

	if (parse(a_number, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_bad_integer)
	using namespace h119::syntx;

	std::string test = "023";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= integer(integer::type::unsigned_int);

	if (parse(a_number, context, result, root, error)) {
		std::cout << "First: " << *result.first << " Second: " << *result.second << std::endl;

		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << "Expected error: " << std::endl;
	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return true;

end_testcase
