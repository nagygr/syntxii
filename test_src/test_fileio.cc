#include <fstream>         // std::fstream
#include <iostream>        // std::cout
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>
#include <h119/util/format.h>
#include <h119/util/logger.h>

using namespace h119::util;
using namespace h119::syntx;

start_testcase(test_fileio_listofnumbers)
	std::string test = read_file_to_string("test_data/list_of_numbers");

	log_3("The read contents: \"%0\"", test);

	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule list_of_numbers("list_of_numbers"), number("number");
	rule oct_num("oct_num"), oct_prefix("oct_prefix"), oct_digit("oct_digit");
	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");

	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	// Parser grammar
	list_of_numbers <<= -number << +(character(",") << -number);
	number <<= oct_num | hexa_num | integer();

	oct_num <<= oct_prefix << +oct_digit;
	oct_prefix <<= character("0");
	oct_digit <<= character("01234567");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// Parsing
	if (parse(list_of_numbers, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		std::fstream fs("test_data/test_listofnumbers.dot", std::fstream::out);
		create_dot_tree(root, fs);
		fs.close();

		return true;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_fileio_listofnumbers_withwhitespacesatendoffile)
	std::string test = read_file_to_string("test_data/list_of_numbers_space");

	log_3("The read contents: \"%0\"", test);

	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule list_of_numbers("list_of_numbers"), number("number");
	rule oct_num("oct_num"), oct_prefix("oct_prefix"), oct_digit("oct_digit");
	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");

	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	// Parser grammar
	list_of_numbers <<= -number << +(character(",") << -number) << -epsilon();
	number <<= oct_num | hexa_num | integer();

	oct_num <<= oct_prefix << +oct_digit;
	oct_prefix <<= character("0");
	oct_digit <<= character("01234567");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// Parsing
	if (parse(list_of_numbers, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		std::fstream fs("test_data/test_listofnumbers.dot", std::fstream::out);
		create_dot_tree(root, fs);
		fs.close();

		return true;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase
