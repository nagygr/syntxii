#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_hexa)
	using namespace h119::syntx;

	std::string test = "0x12fa";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	if (parse(hexa_num, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;
end_testcase

start_testcase(test_syntax_error)
	using namespace h119::syntx;

	std::string test = "0ax12fa";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");
	std::shared_ptr<node> root = std::make_shared<node>("root");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	if (parse(hexa_num, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return *std::get<1>(error) == 'a';
end_testcase
