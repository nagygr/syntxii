#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_csv)
	using namespace h119::syntx;

	std::string test = "12, 22, 34, 5\n8, 23, 45, 7\n1, 2, 3, 4";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule value("value"), csv_line("csv_line"), csv_document("csv_document");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	value <<= +character("0123456789");
	csv_line <<=  ~value << *(character(",") << ~value);
	csv_document <<= +-csv_line; 

	if (parse(csv_document, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;
end_testcase


