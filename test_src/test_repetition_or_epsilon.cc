#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_repetition_or_epsilon_simple)
	using namespace h119::syntx;

	std::string test = "apple,banana,cherry";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule fruit_names("fruit_names");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	fruit_names <<= identifier() << *(character(",") << identifier());

	if (parse(fruit_names, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_repetition_or_epsilon_epsilon)
	using namespace h119::syntx;

	std::string test = "apple";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule fruit_names("fruit_names");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	fruit_names <<= identifier() << *(character(",") << identifier());

	if (parse(fruit_names, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

