#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string
#include <fstream>        // std::fstream

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_inrange)
	using namespace h119::syntx;

	std::string test = "cddcdce";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule ince("ince");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	ince <<= +in_range('c', 'e');

	if (parse(ince, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;
end_testcase

start_testcase(test_time)
	using namespace h119::syntx;

	std::string test = "22:34:06";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule time("time"), hours("hours"), separator("separator"), minutes("minutes"), seconds("seconds"), two_digits("two_digits");
	std::shared_ptr<node> root = std::make_shared<node>("root");

	time <<= hours << separator << minutes << separator << seconds;

	hours      <<= two_digits;
	minutes    <<= two_digits;
	seconds    <<= two_digits;

	two_digits <<= in_range('0','9') << in_range('0','9');
	separator  <<= character(":");
	
	if (parse(time, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		std::fstream fs("test_data/test_time.dot", std::fstream::out);
		create_dot_tree(root, fs);
		fs.close();

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;
end_testcase

start_testcase(test_time_with_error)
	using namespace h119::syntx;

	std::string test = "22:3_:06";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule time("time"), hours("hours"), separator("separator"), minutes("minutes"), seconds("seconds"), two_digits("two_digits");
	std::shared_ptr<node> root = std::make_shared<node>("root");

	time <<= hours << separator << minutes << separator << seconds;

	hours      <<= two_digits;
	minutes    <<= two_digits;
	seconds    <<= two_digits;

	two_digits <<= in_range('0','9') << in_range('0','9');
	separator  <<= character(":");
	
	if (parse(time, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return false; // expecting a failure
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return true; // expecting a failure
end_testcase
