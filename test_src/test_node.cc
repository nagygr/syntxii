#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <cstring>         // std::strcmp

#include <h119/testing/tester.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>

start_testcase(test_node)
	using namespace h119::syntx;

	std::string text = "Hello world";
	std::shared_ptr<node> root = std::make_shared<node>("root");
	root->children.push_back(std::make_shared<node>("child", match_range(text.cbegin(), text.cend())));

	print_tree(root);

	return strcmp(root->children[0]->name, "child") == 0;
end_testcase
