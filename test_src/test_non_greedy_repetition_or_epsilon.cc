#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_non_greedy_repetition_or_epsilon_simple)
	using namespace h119::syntx;

	std::string test = "12435623";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule number_ending_in_twenty_three("number_ending_in_twenty_three");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	number_ending_in_twenty_three <<= character("0123456789") * substring("23");

	if (parse(number_ending_in_twenty_three, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_non_greedy_repetition_or_epsilon_without_repeated)
	using namespace h119::syntx;

	std::string test = "23";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule number_ending_in_twenty_three("number_ending_in_twenty_three");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	number_ending_in_twenty_three <<= character("0123456789") * substring("23");

	if (parse(number_ending_in_twenty_three, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase
