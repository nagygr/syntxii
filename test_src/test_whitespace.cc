#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_whitespace_1)
	using namespace h119::syntx;

	std::string test = "   \t\nhello\t\n\n123.43\n\t";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule whitespace_test("whitespace_test"), a_word("a_word"), a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	whitespace_test <<= -a_word << -a_number << -epsilon();
	a_word <<= keyword("hello");
	a_number <<= real();

	if (parse(whitespace_test, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_whitespace_2)
	using namespace h119::syntx;

	std::string test = "   \thello\t  123.43\t\n";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule whitespace_test("whitespace_test"), a_word("a_word"), a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	whitespace_test <<= ~a_word << ~a_number << -epsilon();
	a_word <<= keyword("hello");
	a_number <<= real();

	if (parse(whitespace_test, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase
