#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_option_simple)
	using namespace h119::syntx;

	std::string test = "ab";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule optional("optional");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	optional <<= !character("a") << character("b");

	if (parse(optional, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;
end_testcase


start_testcase(test_option_complex)
	using namespace h119::syntx;

	std::string test = "ab";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule optional("optional");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	optional <<= !character("a") << !character("cde") << !(character("yuib") << character("wert")) << character("b");

	if (parse(optional, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;
end_testcase

