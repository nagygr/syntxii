#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_real_0)
	using namespace h119::syntx;

	std::string test = "-123";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= real();

	if (parse(a_number, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_real_1)
	using namespace h119::syntx;

	std::string test = "-123.23";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= real();

	if (parse(a_number, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_real_2)
	using namespace h119::syntx;

	std::string test = "+.21";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= real();

	if (parse(a_number, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_real_3)
	using namespace h119::syntx;

	std::string test = "-.2E-12";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= real();

	if (parse(a_number, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_real_4)
	using namespace h119::syntx;

	std::string test = "-123.456e+23";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= real();

	if (parse(a_number, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_real_5)
	using namespace h119::syntx;

	std::string test = "-e+23";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= real();

	if (parse(a_number, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched although an error was expected: " << match << std::endl;

		print_tree(root);

		return false;
	}

	std::cout << "Expected error: " << std::endl;
	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return true;
end_testcase

start_testcase(test_real_6)
	using namespace h119::syntx;

	std::string test = "-12e";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("a_number");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= real();

	if (parse(a_number, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched although an error was expected: " << match << std::endl;

		print_tree(root);

		return false;
	}

	std::cout << "Expected error: " << std::endl;
	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return true;
end_testcase

start_testcase(test_real_7)
	using namespace h119::syntx;

	std::string test = "+,21, ,4, 243,4e-4";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule numbers("numbers");
	rule real_comma("real_comma");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	real_comma <<= real(real::decimal_mark::comma);
	numbers <<= -real_comma << +(-character(",") << -real_comma);

	if (parse(numbers, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase
