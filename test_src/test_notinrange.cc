#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_notinrange)
	using namespace h119::syntx;

	std::string test = "cddcdc";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule notab("notab");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	notab <<= +not_in_range('a', 'b');

	if (parse(notab, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;
end_testcase

