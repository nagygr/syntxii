#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string
#include <fstream>         // std::fstream

#include <h119/testing/tester.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/syntx.h>

start_testcase(test_hexa)
	using namespace h119::syntx;

	// TEST INPUT
	std::string test = "0x12fa";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;
	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator ghexa_num("hexa_num");
	
	// PARSER GRAMMAR
	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// GENERATOR GRAMMAR
	ghexa_num <<= print_value("hexa_prefix") << +print_value("hexa_digit");

	if (hexa_num.match(context, result, root, error) && context.first == context.second) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = ghexa_num.generate(the_generator_range);

		return match == test && generation_done;
	}
	else {
		std::cout << "Didn't match" << std::endl;
		return false;
	}
end_testcase

start_testcase(list_of_numbers_simple)
	using namespace h119::syntx;

	// TEST INPUT
	std::string test = "0x12fa,0345,012,0xc001";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;
	rule list_of_numbers("list_of_numbers"), number("number");
	rule oct_num("oct_num"), oct_prefix("oct_prefix"), oct_digit("oct_digit");
	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator glist_of_numbers("list_of_numbers"), gnumber("number");
	
	// PARSER GRAMMAR
	list_of_numbers <<= number << +(character(",") << number);
	number <<= oct_num | hexa_num;

	oct_num <<= oct_prefix << +oct_digit;
	oct_prefix <<= character("0");
	oct_digit <<= character("01234567");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// GENERATOR GRAMMAR
	glist_of_numbers <<= +gnumber;
	gnumber <<= (print_value("hexa_num") | print_value("oct_num")) << print_text(" ");

	if (list_of_numbers.match(context, result, root, error) && context.first == context.second) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		std::fstream fs("test_data/list_of_numbers_simple.dot", std::fstream::out);
		create_dot_tree(root, fs);
		fs.close();

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = glist_of_numbers.generate(the_generator_range);

		return match == test && generation_done;
	}
	else {
		std::cout << "Didn't match" << std::endl;
		return false;
	}
end_testcase

start_testcase(list_of_numbers_comma_fail)
	using namespace h119::syntx;

	// TEST INPUT
	std::string test = "0x12fa,0345,012,0xc001";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;
	rule list_of_numbers("list_of_numbers"), number("number");
	rule oct_num("oct_num"), oct_prefix("oct_prefix"), oct_digit("oct_digit");
	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator glist_of_numbers("list_of_numbers"), gnumber("number");
	
	// PARSER GRAMMAR
	list_of_numbers <<= number << +(character(",") << number);
	number <<= oct_num | hexa_num;

	oct_num <<= oct_prefix << +oct_digit;
	oct_prefix <<= character("0");
	oct_digit <<= character("01234567");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// GENERATOR GRAMMAR
	glist_of_numbers <<= +gnumber;
	gnumber <<= (print_value("hexa_num") | print_value("oct_num")) << print_text(", ");

	if (list_of_numbers.match(context, result, root, error) && context.first == context.second) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		std::fstream fs("test_data/list_of_numbers_simple.dot", std::fstream::out);
		create_dot_tree(root, fs);
		fs.close();

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = glist_of_numbers.generate(the_generator_range);

		return match == test && generation_done;
	}
	else {
		std::cout << "Didn't match" << std::endl;
		return false;
	}
end_testcase

start_testcase(list_of_numbers)
	using namespace h119::syntx;

	// TEST INPUT
	std::string test = "0x12fa,0345,012,0xc001";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;
	rule list_of_numbers("list_of_numbers"), number("number");
	rule oct_num("oct_num"), oct_prefix("oct_prefix"), oct_digit("oct_digit");
	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator glist_of_numbers("list_of_numbers"), gfirst_number("number");
	generator gother_numbers("number");
	
	// PARSER GRAMMAR
	list_of_numbers <<= number << +(character(",") << number);
	number <<= oct_num | hexa_num;

	oct_num <<= oct_prefix << +oct_digit;
	oct_prefix <<= character("0");
	oct_digit <<= character("01234567");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// GENERATOR GRAMMAR
	glist_of_numbers <<= gfirst_number << *gother_numbers;
	gfirst_number <<= print_value("hexa_num") | print_value("oct_num");
	gother_numbers <<= 
		(
			if_next_node("hexa_num", print_text(", ")) << print_value("hexa_num")
		)
		| 
		(
			// Testing that any number of arguments can be added to if_next_node:
			if_next_node("oct_num", print_text(","), print_text(" ")) << print_value("oct_num")
		);

	if (list_of_numbers.match(context, result, root, error) && context.first == context.second) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = glist_of_numbers.generate(the_generator_range);

		return match == test && generation_done;
	}
	else {
		std::cout << "Didn't match" << std::endl;
		return false;
	}
end_testcase

start_testcase(test_list_of_numbers_with_lambda)
	using namespace h119::syntx;

	// TEST INPUT
	std::string test = "0x12fa,0345,012,0xc001";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;
	rule list_of_numbers("list_of_numbers"), number("number");
	rule oct_num("oct_num"), oct_prefix("oct_prefix"), oct_digit("oct_digit");
	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator glist_of_numbers("list_of_numbers"), gnumber("number");
	std::vector<int> the_numbers;
	int a_number;
	std::stringstream ss;

	// PARSER GRAMMAR
	list_of_numbers <<= number << +(character(",") << number);
	number <<= oct_num | hexa_num;

	oct_num <<= oct_prefix << +oct_digit;
	oct_prefix <<= character("0");
	oct_digit <<= character("01234567");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// GENERATOR GRAMMAR
	glist_of_numbers <<= +gnumber;
	gnumber <<=
		perform(
			"hexa_num",
			[&the_numbers, &a_number, &ss](generator_range &context) -> bool {
				match_range match = (**context.first).get_match_range();
				ss.clear();
				ss << std::hex << std::string(match.first, match.second);
				ss >> a_number;
				the_numbers.push_back(a_number);

				++context.first;
				return true;
			}
		)
		|
		perform(
			"oct_num",
			[&the_numbers, &a_number, &ss](generator_range &context) -> bool {
				match_range match = (**context.first).get_match_range();
				ss.clear();
				ss << std::oct << std::string(match.first, match.second);
				ss >> a_number;
				the_numbers.push_back(a_number);

				++context.first;
				return true;
			}
		);

	if (list_of_numbers.match(context, result, root, error) && context.first == context.second) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = glist_of_numbers.generate(the_generator_range);

		if (generation_done) {
			for (auto &i: the_numbers) std::cout << i << std::endl;
		}

		return match == test && generation_done;
	}
	else {
		std::cout << "Didn't match" << std::endl;
		return false;
	}
end_testcase

start_testcase(test_list_of_numbers_with_error)
	using namespace h119::syntx;

	// TEST INPUT
	std::string test = "0x12fa,0345,a012,0xc001";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;
	rule list_of_numbers("list_of_numbers"), number("number");
	rule oct_num("oct_num"), oct_prefix("oct_prefix"), oct_digit("oct_digit");
	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator glist_of_numbers("list_of_numbers"), gfirst_number("number");
	generator gother_numbers("number");
	
	// PARSER GRAMMAR
	list_of_numbers <<= number << +(character(",") << number);
	number <<= oct_num | hexa_num;

	oct_num <<= oct_prefix << +oct_digit;
	oct_prefix <<= character("0");
	oct_digit <<= character("01234567");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// GENERATOR GRAMMAR
	glist_of_numbers <<= gfirst_number << *gother_numbers;
	gfirst_number <<= print_value("hexa_num") | print_value("oct_num");
	gother_numbers <<= 
		(
			if_next_node("hexa_num", print_text(", ")) << print_value("hexa_num")
		)
		| 
		(
			// Testing that any number of arguments can be added to if_next_node:
			if_next_node("oct_num", print_text(","), print_text(" ")) << print_value("oct_num")
		);

	if (list_of_numbers.match(context, result, root, error) && context.first == context.second) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = glist_of_numbers.generate(the_generator_range);

		return !(match == test && generation_done);
	}
	else {
		std::cout << "Didn't match" << std::endl;
		std::cout << error_message(match_range(test.begin(), test.end()), error) << std::endl;
		return true;
	}
end_testcase

start_testcase(list_of_numbers_with_extra_rule)
	using namespace h119::syntx;

	// TEST INPUT
	std::string test = "0x12fa,0345,012,0xc001";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;
	rule list_of_numbers("list_of_numbers"), number("number");
	rule comma_number("comma_number");
	rule oct_num("oct_num"), oct_prefix("oct_prefix"), oct_digit("oct_digit");
	rule hexa_num("hexa_num"), hexa_prefix("hexa_prefix"), hexa_digit("hexa_digit");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator glist_of_numbers("list_of_numbers");
	generator gcomma_number("comma_number"), gnumber("number");
	
	// PARSER GRAMMAR
	list_of_numbers <<= number << +(comma_number);
	comma_number <<= character(",") << number;
	number <<= oct_num | hexa_num;

	oct_num <<= oct_prefix << +oct_digit;
	oct_prefix <<= character("0");
	oct_digit <<= character("01234567");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// GENERATOR GRAMMAR
	glist_of_numbers <<= gnumber << *gcomma_number;
	gnumber <<= print_value("hexa_num") | print_value("oct_num");
	gcomma_number <<= print_text(", ") << gnumber;

	if (list_of_numbers.match(context, result, root, error) && context.first == context.second) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		std::fstream fs("test_data/list_of_numbers_with_extra_rule.dot", std::fstream::out);
		create_dot_tree(root, fs);
		fs.close();

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = glist_of_numbers.generate(the_generator_range);

		return match == test && generation_done;
	}
	else {
		std::cout << "Didn't match" << std::endl;
		return false;
	}
end_testcase

start_testcase(list_of_numbers_with_unnamed_rules)
	using namespace h119::syntx;

	// TEST INPUT
	std::string test = "0x12fa,0345,012,0xc001";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;

	rule list_of_numbers("list_of_numbers"), number("number");
	rule oct_num("oct_num"), oct_prefix, oct_digit;
	rule hexa_num("hexa_num"), hexa_prefix, hexa_digit;

	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator glist_of_numbers("list_of_numbers"), gfirst_number("number");
	generator gother_numbers("number");

	// PARSER GRAMMAR
	list_of_numbers <<= number << +(character(",") << number);
	number <<= oct_num | hexa_num;

	oct_num <<= oct_prefix << +oct_digit;
	oct_prefix <<= character("0");
	oct_digit <<= character("01234567");

	hexa_num <<= hexa_prefix << +hexa_digit;
	hexa_prefix <<= character("0") << character("x");
	hexa_digit <<= character("0123456789abcdef");

	// GENERATOR GRAMMAR
	glist_of_numbers <<= gfirst_number << *gother_numbers;
	gfirst_number <<= print_value("hexa_num") | print_value("oct_num");
	gother_numbers <<=
		(
			if_next_node("hexa_num", print_text(", ")) << print_value("hexa_num")
		)
		|
		(
			// Testing that any number of arguments can be added to if_next_node:
			if_next_node("oct_num", print_text(","), print_text(" ")) << print_value("oct_num")
		);

	if (list_of_numbers.match(context, result, root, error) && context.first == context.second) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = glist_of_numbers.generate(the_generator_range);

		return match == test && generation_done;
	}
	else {
		std::cout << "Didn't match" << std::endl;
		return false;
	}
end_testcase
