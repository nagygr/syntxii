#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_epsilon)
	using namespace h119::syntx;

	std::string test = "-123";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule a_number("integer");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	a_number <<= integer() << epsilon();

	if (parse(a_number, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

