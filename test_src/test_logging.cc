#include <h119/testing/tester.h>
#include <h119/util/logger.h>

start_testcase(test_logging)
	log_1("Hello %0", 1);
	log_2("Hello %0", 2);
	log_3("Hello %0", 3);
	log_4("Hello %0", 4);
	log_5("Hello %0", 5);

	return true;
end_testcase
