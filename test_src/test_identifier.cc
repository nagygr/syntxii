#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_identifier)
	using namespace h119::syntx;

	std::string test = "an_idenitifier";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule idf("identifier");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	idf <<= identifier();

	if (parse(idf, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_bad_identifier)
	using namespace h119::syntx;

	std::string test = "0not_an_idenitifier";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule variable_name("variable_name");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	variable_name <<= identifier();

	if (parse(variable_name, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << "Expected error: " << std::endl;
	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return true;

end_testcase
