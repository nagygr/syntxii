#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>
#include <h119/util/format.h>
#include <h119/util/logger.h>


start_testcase(test_time_generation)
	using namespace h119::syntx;
	
	// TEST INPUT
	std::string test = "22:34:06";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;
	rule time("time"), hours("hours"), minutes("minutes"), seconds("seconds"); 
	rule two_digits("two_digits"), separator("separator"); 
	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator gtime("time"), ghours("hours"), gminutes("minutes"), gseconds("seconds");

	// PARSER GRAMMAR
	time <<= hours << separator << minutes << separator << seconds;

	hours      <<= two_digits;
	minutes    <<= two_digits;
	seconds    <<= two_digits;

	two_digits <<= character("0123456789") << character("0123456789");
	separator  <<= character(":");

	// GENERATOR GRAMMAR
	gtime <<= ghours << skip("separator") << gminutes << skip("separator") << gseconds;

	ghours <<= print_value("two_digits") << print_text(" hours ");
	gminutes <<= print_value("two_digits") << print_text(" minutes ");
	gseconds <<= print_value("two_digits") << print_text(" seconds.\n");

	if (parse(time, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = gtime.generate(the_generator_range);

		if (generation_done) {
			std::cout << "Generation done" << std::endl;
		}

		return match == test && generation_done;
	}
	else {
		std::cout << "Didn't match" << std::endl;
		return false;
	}
end_testcase
