#include <iostream>        // std::cout, std::boolalpha
#include <memory>          // std::shared_ptr
#include <string>          // std::string
#include <sstream>         // std::stringstream
#include <fstream>         // std::fstream
#include <map>             // std::map
#include <stack>           // std::stack

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>
#include <h119/util/format.h>
#include <h119/util/logger.h>

using namespace h119::util;

class json_boolean;
class json_number;
class json_text;
class json_object;
class json_boolean_array;
class json_number_array;
class json_text_array;
class json_object_array;

class json_visitor {
	public:
		virtual void visit(json_boolean &n) = 0;
		virtual void visit(json_number &n) = 0;
		virtual void visit(json_text &n) = 0;
		virtual void visit(json_object &n) = 0;
		virtual void visit(json_boolean_array &n) = 0;
		virtual void visit(json_number_array &n) = 0;
		virtual void visit(json_text_array &n) = 0;
		virtual void visit(json_object_array &n) = 0;
};

class json_base_object {
	public:
		virtual void accept(json_visitor &visitor) = 0;
		virtual ~json_base_object() {}
};

template <typename type>
type get_value_as(std::string const &value) {
	std::stringstream stream;
	type result;

	stream << std::boolalpha;

	stream << value;
	stream >> result;

	log_3("get_value_as input: %0 result: %1", value, result);

	return result;
}

class json_number : public json_base_object {
	private:
		double value;

	public:
		json_number(std::string const &value) {
			this->value = get_value_as<double>(value);
		}

		double get_value() {
			return value;
		}

		virtual void accept(json_visitor &visitor) override {
			visitor.visit(*this);
		}
};

class json_boolean : public json_base_object {
	private:
		bool value;

	public:
		json_boolean(std::string const &value) {
			this->value = get_value_as<bool>(value);
		}

		bool get_value() {
			return value;
		}

		virtual void accept(json_visitor &visitor) override {
			visitor.visit(*this);
		}
};
		
class json_text : public json_base_object {
	private:
		std::string value;

	public:
		json_text(std::string const &value) {
			this->value = value;
		}

		std::string &get_value() {
			return value;
		}

		virtual void accept(json_visitor &visitor) override {
			visitor.visit(*this);
		}
};

class json_object : public json_base_object {
	private:
		std::map<std::string, std::shared_ptr<json_base_object>> members;

	public:
		void add_member(std::string name, std::shared_ptr<json_base_object> value) {
			log_3("Object member added (%0)", name);
			members.insert(std::make_pair(name, value));
		}

		std::map<std::string, std::shared_ptr<json_base_object>> &get_members() {
			return members;
		}

		virtual void accept(json_visitor &visitor) override {
			visitor.visit(*this);
		}
};

class json_boolean_array : public json_base_object {
	private:
		std::vector<std::shared_ptr<json_boolean>> values;

	public:
		virtual std::shared_ptr<json_boolean> add_element(std::string const &value) {
			values.push_back(std::make_shared<json_boolean>(value));
			return values.back();
		}
		
		std::vector<std::shared_ptr<json_boolean>> &get_values() {
			return values;
		}
		
		virtual void accept(json_visitor &visitor) override {
			visitor.visit(*this);
		}
};

class json_number_array : public json_base_object {
	private:
		std::vector<std::shared_ptr<json_number>> values;

	public:
		virtual std::shared_ptr<json_number> add_element(std::string const &value) {
			values.push_back(std::make_shared<json_number>(value));
			return values.back();
		}
		
		std::vector<std::shared_ptr<json_number>> &get_values() {
			return values;
		}

		virtual void accept(json_visitor &visitor) override {
			visitor.visit(*this);
		}
};

class json_text_array : public json_base_object {
	private:
		std::vector<std::shared_ptr<json_text>> values;

	public:
		virtual std::shared_ptr<json_text> add_element(std::string const &value) {
			values.push_back(std::make_shared<json_text>(value));
			return values.back();
		}
		
		std::vector<std::shared_ptr<json_text>> &get_values() {
			return values;
		}

		virtual void accept(json_visitor &visitor) override {
			visitor.visit(*this);
		}
};

class json_object_array : public json_base_object {
	private:
		std::vector<std::shared_ptr<json_object>> values;

	public:
		virtual std::shared_ptr<json_object> add_element() {
			values.push_back(std::make_shared<json_object>());
			return values.back();
		}
		
		std::vector<std::shared_ptr<json_object>> &get_values() {
			return values;
		}

		virtual void accept(json_visitor &visitor) override {
			visitor.visit(*this);
		}
};

class json_to_xml_visitor : public json_visitor {
	private:
		size_t tab_depth;

	public:
		json_to_xml_visitor() : tab_depth(0) {}


		virtual void visit(json_boolean &n) {
			print_tabs();
			std::cout << format("<json_boolean value=%0 />\n", n.get_value());
		}

		virtual void visit(json_number &n) {
			print_tabs();
			std::cout << format("<json_number value=%0 />\n", n.get_value());
		}

		virtual void visit(json_text &n) {
			print_tabs();
			std::cout << format("<json_text value=%0/>\n", n.get_value());
		}

		virtual void visit(json_object &n) {
			print_tabs();
			std::cout << "<json_object>" << std::endl;

			++tab_depth;

			auto &members = n.get_members();
			for (auto &m: members) {
				print_tabs();
				std::cout << format("<object_member name=%0>\n", m.first);

				++tab_depth;
				m.second->accept(*this);

				--tab_depth;
				print_tabs();
				std::cout << "</object_member>\n";
			}

			--tab_depth;
			print_tabs();
			std::cout << "</json_object>" << std::endl;
		}

		virtual void visit(json_boolean_array &n) {
			print_tabs();
			std::cout << "<json_boolean_array>" << std::endl;

			++tab_depth;

			auto &values = n.get_values();
			for (auto &m: values) {
				m->accept(*this);
			}

			--tab_depth;
			print_tabs();
			std::cout << "</json_boolean_array>" << std::endl;
		}

		virtual void visit(json_number_array &n) {
			print_tabs();
			std::cout << "<json_number_array>" << std::endl;

			++tab_depth;

			auto &values = n.get_values();
			for (auto &m: values) {
				m->accept(*this);
			}

			--tab_depth;
			print_tabs();
			std::cout << "</json_number_array>" << std::endl;
		}

		virtual void visit(json_text_array &n) {
			print_tabs();
			std::cout << "<json_text_array>" << std::endl;

			++tab_depth;

			auto &values = n.get_values();
			for (auto &m: values) {
				m->accept(*this);
			}

			--tab_depth;
			print_tabs();
			std::cout << "</json_text_array>" << std::endl;
		}

		virtual void visit(json_object_array &n) {
			print_tabs();
			std::cout << "<json_object_array>" << std::endl;

			++tab_depth;

			auto &values = n.get_values();
			for (auto &m: values) {
				m->accept(*this);
			}

			--tab_depth;
			print_tabs();
			std::cout << "</json_object_array>" << std::endl;
		}

	private:
		void print_tabs(std::ostream &stream = std::cout) {
			for (size_t i = 0; i <= tab_depth; ++i) {
				stream << "\t";
			}
		}
};

template <typename type>
class simple_json_variable_adder {
	private:
		std::stack<std::shared_ptr<json_base_object>> &object_stack;
		std::string &current_name;
	
	public:
		simple_json_variable_adder(std::stack<std::shared_ptr<json_base_object>> &object_stack, std::string &current_name) :
			object_stack(object_stack),
			current_name(current_name) {}

		bool operator()(h119::syntx::generator_range &context) {
			using namespace h119::syntx;
			match_range match = (**context.first).get_match_range();
			std::shared_ptr<json_base_object> current_base = object_stack.top();
			json_object &current_object = dynamic_cast<json_object&>(*current_base);
			current_object.add_member(current_name, std::make_shared<type>(std::string(match.first, match.second)));
			++context.first;
			log_3("Simple variable value set (%0)", std::string(match.first, match.second));
			return true;
		}
};

template <typename array_type>
class json_array_adder {
	private:
		std::stack<std::shared_ptr<json_base_object>> &object_stack;
		std::string &current_name;

	public:
		json_array_adder(
			std::stack<std::shared_ptr<json_base_object>> &object_stack,
			std::string &current_name
		) :
			object_stack(object_stack),
			current_name(current_name) {}

		bool operator()(h119::syntx::generator_range &context) {
			using namespace h119::syntx;
			std::shared_ptr<json_base_object> new_array = std::make_shared<array_type>();
			std::shared_ptr<json_base_object> current_base = object_stack.top();
			
			// if current element is an object:
			if (dynamic_cast<json_object*>(&(*current_base))) {
				json_object &current_object = dynamic_cast<json_object &>(*current_base);
				current_object.add_member(current_name, new_array);

			}
			else {
				log_1("Array of arrays is not handled at the moment");
				return false;
			}

			object_stack.push(new_array);
			++context.first;
			log_3("New array created and pushed on stack");
			return true;
		}
};

template <typename array_type>
class json_array_element_adder {
	private:
		std::stack<std::shared_ptr<json_base_object>> &object_stack;

	public:
		json_array_element_adder(std::stack<std::shared_ptr<json_base_object>> &object_stack) :
			object_stack(object_stack) {}

		bool operator()(h119::syntx::generator_range &context) {
			using namespace h119::syntx;
			match_range match = (**context.first).get_match_range();
			std::shared_ptr<json_base_object> current_base = object_stack.top();
			array_type &current_array = dynamic_cast<array_type&>(*current_base);
			current_array.add_element(std::string(match.first, match.second));
			++context.first;
			log_3("Array element variable value set (%0)", std::string(match.first, match.second));
			return true;
		}
};

template <typename array_type>
class json_array_remover {
	private:
		std::stack<std::shared_ptr<json_base_object>> &object_stack;

	public:
		json_array_remover(std::stack<std::shared_ptr<json_base_object>> &object_stack) :
			object_stack(object_stack) {}

		bool operator()(h119::syntx::generator_range &context) {
			using namespace h119::syntx;
			object_stack.pop();
			++context.first;
			log_3("Array popped from stack");
			return true;
		}
};

start_testcase(test_json)
	using namespace h119::syntx;

	std::string test = R"(
		{
			"g_force": 9.82,
			"pi": 3.14,
			"name": "Descartes",
			"fantastic": true,
			"inner_object" : {
				"insider_joke": false,
				"array_of_texts": ["apple", "banana"]
			},
			"array_of_ints": [12, 34, 123, 67],
			"array_of_bools": [true, false, false, true],
			"object_array": [
				{
					"name": "Jupiter"
				},
				{
					"name": "Pluto"
				},
				{
					"name": "Earth",
					"moons": ["moon"]
				}
			]
		}
	)";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule json_object_rule("json_object_rule"), name_value_pair("name_value_pair");
	rule name("name"), number_rule("number_rule"), text_rule("text_rule"), boolean_rule("boolean_rule");
	rule number_array_rule("number_array_rule"), text_array_rule("text_array_rule"), boolean_array_rule("boolean_array_rule");
	rule object_array_rule("object_array_rule");
	rule start_of_object("start_of_object"), end_of_object("end_of_object"), start_of_array("start_of_array"), end_of_array("end_of_array");

	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	// Parser grammar
	json_object_rule <<= -start_of_object << -name_value_pair << *(-character(",") << -name_value_pair) << -end_of_object << -epsilon();

	start_of_object <<= -character("{");
	end_of_object <<= -character("}");
	
	name_value_pair <<= -name << -character(":") << (
		-number_rule | -text_rule | -boolean_rule | -json_object_rule |
		-number_array_rule | -text_array_rule | -boolean_array_rule | -object_array_rule
	);
	
	name <<= -string();
	boolean_rule <<= -keyword("true") | -keyword("false");
	number_rule <<= -real();
	text_rule <<= -string();

	start_of_array <<= -character("[");
	end_of_array <<= -character("]");
	
	number_array_rule <<= -start_of_array << -number_rule << *(-character(",") << -number_rule) << -end_of_array;
	text_array_rule <<= -start_of_array << -text_rule << *(-character(",") << -text_rule) << -end_of_array;
	boolean_array_rule <<= -start_of_array << -boolean_rule << *(-character(",") << -boolean_rule) << -end_of_array;
	object_array_rule <<= -start_of_array << -json_object_rule << *(-character(",") << -json_object_rule) << -end_of_array;

	// Generator prerequisites && grammar
	generator generate_json_object("json_object_rule");
	generator generate_name_value_pair("name_value_pair");
	generator generate_number_array("number_array_rule");
	generator generate_boolean_array("boolean_array_rule");
	generator generate_text_array("text_array_rule");
	generator generate_object_array("object_array_rule");
	generator generate_object_array_element("json_object_rule");

	std::shared_ptr<json_base_object> top_object;
	std::stack<std::shared_ptr<json_base_object>> object_stack;
	std::string current_name;

	generate_json_object <<= 
						perform(
							"start_of_object",
							[&object_stack, &top_object, &current_name](generator_range &context) -> bool {
								std::shared_ptr<json_base_object> new_object = std::make_shared<json_object>();
								if (object_stack.empty()) {
									log_3("First object created and set as top object");
									top_object = new_object;
								}
								else {
									log_3("Inner object created and added as a child to its parent");
									std::shared_ptr<json_base_object> current_base = object_stack.top();
									json_object &current_object = dynamic_cast<json_object&>(*current_base);
									current_object.add_member(current_name, new_object);
								}
								object_stack.push(new_object);
								++context.first;
								log_3("New object created and pushed on stack");
								return true;
								}
					  	)
					<< +generate_name_value_pair
					<< 
						perform (
							"end_of_object",
							[&object_stack](generator_range &context) -> bool {
								object_stack.pop();
								++context.first;
								log_3("Object popped from stack");
								return true;
							}
						);

	generate_name_value_pair	<<= 
						perform (
							"name",
							[&object_stack, &current_name](generator_range &context) -> bool {
								match_range match = (**context.first).get_match_range();
								current_name = std::string(match.first, match.second);
								++context.first;
								log_3("Member name processed (%0)", current_name);
								return true;
							}
						)
					<<
						(
							perform(
								"number_rule",
								simple_json_variable_adder<json_number>(object_stack, current_name)
							)
							|
							perform(
								"boolean_rule",
								simple_json_variable_adder<json_boolean>(object_stack, current_name)
							)
							|
							perform(
								"text_rule",
								simple_json_variable_adder<json_text>(object_stack, current_name)
							)
							|
							generate_json_object
							|
							generate_number_array
							|
							generate_boolean_array
							|
							generate_text_array
							|
							generate_object_array
						);

	generate_number_array	<<=
						perform (
							"start_of_array",
							json_array_adder<json_number_array>(object_stack, current_name)
						)
					<< 
						+perform(
							"number_rule",
							json_array_element_adder<json_number_array>(object_stack)
						)
					<<
						perform (
							"end_of_array",
							json_array_remover<json_number_array>(object_stack)
						);

	generate_boolean_array	<<=
						perform (
							"start_of_array",
							json_array_adder<json_boolean_array>(object_stack, current_name)
						)
					<< 
						+perform(
							"boolean_rule",
							json_array_element_adder<json_boolean_array>(object_stack)
						)
					<<
						perform (
							"end_of_array",
							json_array_remover<json_boolean_array>(object_stack)
						);

	generate_text_array	<<=
						perform (
							"start_of_array",
							json_array_adder<json_text_array>(object_stack, current_name)
						)
					<< 
						+perform(
							"text_rule",
							json_array_element_adder<json_text_array>(object_stack)
						)
					<<
						perform (
							"end_of_array",
							json_array_remover<json_text_array>(object_stack)
						);

	generate_object_array <<=
						perform (
							"start_of_array",
							json_array_adder<json_object_array>(object_stack, current_name)
						)
					<<
						+generate_object_array_element
					<<
						perform (
							"end_of_array",
							json_array_remover<json_object_array>(object_stack)
						);

	/**
	 * Here the start_of_object creates a new object and adds it as an element of the object_array
	 * that is on top of the stack, then pushes the object to the stack. This way the generate_name_value_pair
	 * will add the object members to the object that is on top of the stack. The end_of_object step pops
	 * one element from the stack, so the object is popped and the array gets to the top again so that the
	 * next object can be added to it, and so on.
	 */
	generate_object_array_element <<=
						perform (
							"start_of_object",
							[&object_stack, &top_object](generator_range &context) -> bool {
								std::shared_ptr<json_base_object> current_base = object_stack.top();
								json_object_array &current_object_array = dynamic_cast<json_object_array&>(*current_base);
								std::shared_ptr<json_base_object> new_object = current_object_array.add_element();
								object_stack.push(new_object);
								++context.first;
								log_3("New object created and pushed on stack and into object array");
								return true;
							}
						)
					<< +generate_name_value_pair
					<< 
						perform (
							"end_of_object",
							[&object_stack](generator_range &context) -> bool {
								object_stack.pop();
								++context.first;
								log_3("Object popped from stack");
								return true;
							}
						);

	// Parsing
	if (parse(json_object_rule, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		std::fstream fs("test_data/test_json.dot", std::fstream::out);
		create_dot_tree(root, fs);
		fs.close();

		std::cout << "Starting the generation:" << std::endl;
		std::cout << "========================" << std::endl;
		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = generate_json_object.generate(the_generator_range);

		if (generation_done) {
			std::cout << std::endl;
			std::cout << "Starting visiting the generated structure:" << std::endl;
			std::cout << "==========================================" << std::endl;
			json_to_xml_visitor sv;
			top_object->accept(sv);
		}

		return match == test && generation_done;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

