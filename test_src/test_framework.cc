#include <iostream>

#include <h119/testing/tester.h>

start_testcase(test_environment)
	std::cout << "Hello" << std::endl;
	return true;
end_testcase

start_testcase(test_deliberate_failure)
	std::cout << "This test fails deliberately" << std::endl;
	return false;
end_testcase
