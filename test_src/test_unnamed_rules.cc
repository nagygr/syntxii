#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/syntx.h>

/*
 * Testing whether a collapsed AST works well with generators.
 * The tree is collapsed because the "middleman" rule "member"
 * doesn't have a name and thus is not included in the AST but its
 * children: "data_element" and "sexpr" (this one due to recursion) are
 * included. This means that, compared to the natural AST, the layer of the
 * "member" node is collapsed. In the AST these layers simply don't appear
 * and thus the generators have to be written in a way that only the final tree
 * is considered and parsed.
 *
 * The if_next_node trick is only needed to have spaces between the elements. It
 * could be left out.
 */
start_testcase(test_unnamed_rules)
	using namespace h119::syntx;

	// TEST INPUT
	std::string test = "( apple 3.14 ( \"Hello world\" (pi) (golden) ))";

	// PARSER GRAMMAR PREREQUISITES
	match_range context = {test.begin(), test.end()}, result;
	
	rule sexpr("sexpr"), opening_brace, closing_brace, member, data_element("data_element");
	
	std::shared_ptr<node> root = std::make_shared<node>("root");
	syntax_error error;

	// GENERATOR GRAMMAR PREREQUISITES
	generator gsexpr("sexpr"), gdata_element("data_element");
	
	// PARSER GRAMMAR
	sexpr <<= -opening_brace << +(-member) << -closing_brace;
	opening_brace <<= character("(");
	closing_brace <<= character(")");
	member <<= data_element | sexpr;
	data_element <<= real() | identifier() | string();

	// GENERATOR GRAMMAR
	gsexpr <<= 
		*(
			(
				if_next_node("data_element", print_text(" ")) << 
				print_value("data_element") 
			)
			| gsexpr
		);


	if (sexpr.match(context, result, root, error) && context.first == context.second) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		generator_range the_generator_range(root->cbegin(), root->cend());
		bool generation_done = gsexpr.generate(the_generator_range);

		return match == test && generation_done;
	}
	else {
		std::cout << "Didn't match" << std::endl;
		return false;
	}
end_testcase

