#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/testing/tester.h>
#include <h119/syntx/syntx.h>

start_testcase(test_substring_1)
	using namespace h119::syntx;

	std::string test = "applepeach{}";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule str("substring");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	str <<= substring("apple") << substring("peach") << +character("}{");

	if (parse(str, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase

start_testcase(test_substring_2)
	using namespace h119::syntx;

	std::string test = "applepeach{}";
	match_range context = {test.begin(), test.end()}, result;
	syntax_error error;

	rule str("substring"), apple("apple"), peach("peach");
	std::shared_ptr<node> root = std::make_shared<node>("root");
	
	str <<= apple << peach << +character("}{");
	apple <<= substring("apple");
	peach <<= substring("peach");

	if (parse(str, context, result, root, error)) {
		std::string match = std::string(result.first, result.second);
		std::cout << "Matched: " << match << std::endl;

		print_tree(root);

		return match == test;
	}

	std::cout << error_message({test.begin(), test.end()}, error) << std::endl;
	return false;

end_testcase
