#ifndef H119_SYNTX_EPSILON
#define H119_SYNTX_EPSILON

#include <memory>          // std::shared_ptr
#include <string>          // std::string
#include <utility>         // std::pair

#include <h119/syntx/base_rule.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/character_level_rule.h>
#include <h119/syntx/node.h>

namespace h119 { namespace syntx {

class epsilon : public base_rule {
	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override {
			matching_range = std::make_pair(context.first, context.first);
			return true;
		}

	public:
		virtual std::shared_ptr<base_rule> clone() const override {
			return std::make_shared<epsilon>(*this);
		}
};

}}

#endif // H119_SYNTX_EPSILON

