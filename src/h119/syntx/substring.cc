#include <utility>         // std::pair
#include <iterator>        // std::distance
#include <cstring>         // std::strlen

#include <h119/util/format.h>
#include <h119/util/logger.h>
#include <h119/syntx/substring.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool substring::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	match_range local_context = context;
	char const *word_iterator = the_word;

	while (*word_iterator != '\0' && *word_iterator == *local_context.first) {
		++word_iterator;
		++local_context.first;
	}

	if (
		*word_iterator == '\0' &&
		static_cast<long>(strlen(the_word)) == std::distance(context.first, local_context.first)
	) {
		matching_range = std::make_pair(context.first, local_context.first);
		context = local_context;

		log_3("Substring matched (%0)", std::string(matching_range.first, matching_range.second));
		return true;
	}

	log_5("Failed to match substring at %0", *context.first);
	set_error_message(context.first, error);
	return false;
}

std::shared_ptr<base_rule> substring::clone() const {
	return std::make_shared<substring>(*this);
}

std::string substring::description() const {
	return format("A substring (the word: \"%0\")", the_word);
}

}}

