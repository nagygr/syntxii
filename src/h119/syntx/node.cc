#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr
#include <sstream>         // std::stringstream
#include <iterator>        // std::distance

#include <h119/syntx/node.h>
#include <h119/util/format.h>

namespace h119 { namespace syntx {

void print_tree(std::shared_ptr<node> root, size_t level) {
	if (root) {
		for (size_t i = 0; i < level; ++i) std::cout << "\t";
		match_range range = root->get_match_range();
		std::cout << root->name << "  (" << std::string(range.first, range.second) << ")" << std::endl;
		for (auto &child: root->children) print_tree(child, level + 1);
	}
}

void create_dot_tree_helper(std::shared_ptr<node> root, std::ostream &stream);

void create_dot_tree(std::shared_ptr<node> root, std::ostream &stream) {
	std::string preamble = R"raw(digraph G {
		ranksep=0.25;
		graph [ bgcolor=transparent ]
		node [ style=filled, fillcolor=white, fontname="Sans", fontsize=12, margin=0.025 ]
		)raw";

	std::string closing_mark = "}\n";

	stream << preamble;

	create_dot_tree_helper(root, stream);

	stream << closing_mark;
}

void create_dot_tree_helper(std::shared_ptr<node> root, std::ostream &stream) {
	match_range the_matching_range = root->get_match_range();
	static const int max_value_length = 20;
	std::string value_text;

	if (std::distance(the_matching_range.first, the_matching_range.second) > max_value_length) {
		value_text = std::string(the_matching_range.first, the_matching_range.first + max_value_length);
		value_text[max_value_length - 1] = value_text[max_value_length - 2] = value_text[max_value_length - 3] = '.';
	}
	else value_text = std::string(the_matching_range.first, the_matching_range.second);

	for (auto &c: value_text) {
		if (c == '\n' || c == '\t') c = ' ';
		if (c == '"' || c == '\'') c = '`';
	}

	std::stringstream name, value;
	name << "l" << root.get();
	value << root->name << "\\n" << value_text;

	stream << name.str() << " [label = \"" << value.str() << "\"]" << std::endl;

	for (auto const &child: root->children) {
		std::stringstream child_name;
		child_name << "l" << child.get();
		stream << name.str() << " -> " << child_name.str() << std::endl;
	}

	for (auto &child: root->children)
		create_dot_tree_helper(child, stream);
}

}}
