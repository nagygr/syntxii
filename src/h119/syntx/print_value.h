#ifndef H119_SYNTX_PRINTVALUE
#define H119_SYNTX_PRINTVALUE

#include <string>          // std::string
#include <ostream>         // std::ostream
#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr

#include <h119/syntx/base_generator.h>

namespace h119 { namespace syntx {

class print_value : public base_generator {
	private:
		std::string name;
		std::ostream *stream;

	public:
		print_value(std::string const &name, std::ostream &stream = std::cout) : name(name), stream(&stream) {}
		virtual bool generate(generator_range &context) override;
		virtual std::shared_ptr<base_generator> clone() const override;

};

}}

#endif // H119_SYNTX_PRINTVALUE

