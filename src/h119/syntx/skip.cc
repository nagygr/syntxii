#include <h119/syntx/skip.h>

namespace h119 { namespace syntx {

bool skip::generate(generator_range &context) {
	if (context.first == context.second) return false;

	std::shared_ptr<node> current_node = *context.first;
	if (current_node->name != name) return false;

	++context.first;
	return true;
}

std::shared_ptr<base_generator> skip::clone() const {
	return std::make_shared<skip>(*this);
}

}}

