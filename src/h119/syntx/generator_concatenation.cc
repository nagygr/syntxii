#include <h119/syntx/generator_concatenation.h>

namespace h119 { namespace syntx {

bool generator_concatenation::generate(generator_range &context) {
	generator_range local = context;

	if (lhs->generate(local) && rhs->generate(local)) {
		context = local;

		return true;
	}

	return false;
}

std::shared_ptr<base_generator> generator_concatenation::clone() const {
	return std::make_shared<generator_concatenation>(*this);
}

generator_concatenation operator <<(base_generator const &lhs, base_generator const &rhs) {
	return generator_concatenation(lhs.clone(), rhs.clone());
}

}}

