#include <utility>         // std::pair

#include <h119/util/format.h>
#include <h119/util/string.h>
#include <h119/util/logger.h>
#include <h119/syntx/keyword.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool keyword::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	match_range local_context = context;

	character_range lower{'a', 'z'}, upper{'A', 'Z'}, numeric{'0', '9'};

	char const *word_iterator = the_word;
	while (*word_iterator == *local_context.first) {
		++word_iterator;
		++local_context.first;
	}

	if (
		*word_iterator == '\0' &&
		!in_range(*local_context.first, lower) &&
		!in_range(*local_context.first, upper) &&
		!in_range(*local_context.first, numeric) &&
		!in_set(*local_context.first, extra_characters)
	) {
		matching_range = std::make_pair(context.first, local_context.first);
		context = local_context;

		log_3("Keyword matched (%0)", std::string(matching_range.first, matching_range.second));

		return true;
		
	}
		
	log_5("Failed to match keyword at %0", *context.first);
	set_error_message(context.first, error);
	return false;
}

std::shared_ptr<base_rule> keyword::clone() const {
	return std::make_shared<keyword>(*this);
}

std::string keyword::description() const {
	return format("A keyword (word: %0, extra characters: %1)",
		the_word, 
		extra_characters
	);
}

}}


