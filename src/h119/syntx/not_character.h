#ifndef H119_SYNTX_NOTCHARACTER
#define H119_SYNTX_NOTCHARACTER

#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/syntx/base_rule.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/character_level_rule.h>
#include <h119/syntx/node.h>

namespace h119 { namespace syntx {

class not_character : public base_rule, public character_level_rule {
	private:
		char const *disallowed_characters;
	
	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;
		virtual std::string description() const override;

	public:
		not_character(char const *disallowed_characters) : disallowed_characters(disallowed_characters) {}
		virtual std::shared_ptr<base_rule> clone() const override;
};

}}

#endif // H119_SYNTX_NOTCHARACTER
