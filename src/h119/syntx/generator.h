#ifndef H119_SYNTX_GENERATOR
#define H119_SYNTX_GENERATOR

#include <string>          // std::string
#include <memory>          // std::shared_ptr

#include <h119/syntx/base_generator.h>

namespace h119 { namespace syntx {

class generator : public base_generator {
	private:
		std::string name;
		std::shared_ptr<std::shared_ptr<base_generator>> the_generator;

	public:
		generator(std::string const &name, std::shared_ptr<base_generator> a_generator = nullptr) : name(name), the_generator(new std::shared_ptr<base_generator>(a_generator)) {}
		virtual bool generate(generator_range &context) override;
		virtual std::shared_ptr<base_generator> clone() const override;

		generator &operator <<=(base_generator const &a_generator) {
			*the_generator = a_generator.clone();
			return *this;
		}
};

}}

#endif // H119_SYNTX_GENERATOR

