#ifndef H119_SYNTX_GENERATORREPETITIONOREPSILON
#define H119_SYNTX_GENERATORREPETITIONOREPSILON

#include <memory>          // std::shared_ptr

#include <h119/syntx/base_generator.h>

namespace h119 { namespace syntx {

class generator_repetition_or_epsilon : public base_generator {
	private:
		std::shared_ptr<base_generator> repeated_generator;

	public:
		generator_repetition_or_epsilon(std::shared_ptr<base_generator> rhs) : repeated_generator(rhs) {}
		virtual bool generate(generator_range &context) override;
		virtual std::shared_ptr<base_generator> clone() const override;
};

generator_repetition_or_epsilon operator *(base_generator const &a_generator);

}}

#endif // H119_SYNTX_GENERATORREPETITIONOREPSILON


