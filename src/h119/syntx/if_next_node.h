#ifndef H119_SYNTX_IFNEXTNODE
#define H119_SYNTX_IFNEXTNODE

#include <string>          // std::string
#include <vector>          // std::vector
#include <memory>          // std::shared_ptr

#include <h119/syntx/base_generator.h>

namespace h119 { namespace syntx {

class if_next_node : public base_generator {
	private:
		std::string name;
		std::vector<std::shared_ptr<base_generator>> the_generators;

	public:
		template <typename ...generators>
		if_next_node(std::string const &name, generators const & ...generator_arguments) : name(name) {
			std::shared_ptr<base_generator> generator_array[] = { generator_arguments.clone() ... };
			the_generators.insert(the_generators.begin(), std::begin(generator_array), std::end(generator_array));
		}
		virtual bool generate(generator_range &context) override;
		virtual std::shared_ptr<base_generator> clone() const override;
};

}}

#endif // H119_SYNTX_IFNEXTNODE

