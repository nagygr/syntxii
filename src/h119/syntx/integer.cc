#include <utility>         // std::pair

#include <h119/util/format.h>
#include <h119/util/string.h>
#include <h119/util/logger.h>
#include <h119/syntx/integer.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool integer::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	match_range local_context = context;

	character_range non_zero{'1', '9'}, numeric{'0', '9'};
	char neg_sign = '-', pos_sign = '+', zero = '0';
	bool is_negative = false;

	if (the_integer_type == type::signed_int) {
		is_negative = step_if_matches(neg_sign, local_context.first);
	}

	if (!is_negative) step_if_matches(pos_sign, local_context.first);

	if (step_if_matches(zero, local_context.first)) {
		matching_range = std::make_pair(context.first, local_context.first);
		context = local_context;

		log_3("One digit %0 integer matched (%1)", the_integer_type == type::signed_int ? "signed" : "unsigned", std::string(matching_range.first, matching_range.second));

		return true;
	}
	else if (in_range(*local_context.first, non_zero)) {
		for (++local_context.first; in_range(*local_context.first, numeric); ++local_context.first);

		matching_range = std::make_pair(context.first, local_context.first);
		context = local_context;

		log_3("Multi digit %0 integer matched (%1)", the_integer_type == type::signed_int ? "signed" : "unsigned", std::string(matching_range.first, matching_range.second));

		return true;

	}

	log_5("Failed to match integer at %0", *context.first);
	set_error_message(context.first, error);
	return false;
}

std::shared_ptr<base_rule> integer::clone() const {
	return std::make_shared<integer>(*this);
}

std::string integer::description() const {
	return format("An integer (type: %0)", the_integer_type == type::signed_int ? "signed" : "unsigned");
}

}}

