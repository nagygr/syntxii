#include <string>          // std::string
#include <cstdlib>         // std::abs
#include <iterator>        // std::distance
#include <sstream>         // std::stringstream
#include <fstream>         // std::fstream
#include <stdexcept>       // std::runtime_error

#include <h119/syntx/base_rule.h>
#include <h119/util/logger.h>
#include <h119/util/format.h>

namespace h119 { namespace syntx {

bool base_rule::match(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	match_range local = context, result;

	if (test(local, result, root, error)) {
		context = local;
		matching_range = result;

		log_3("Successful match: %0", std::string(matching_range.first, matching_range.second));
		
		return true;
	}
	
	return false;
}

std::string error_message(match_range const &context, syntax_error const &error) {
	constexpr int offset = 20;

	std::string character_level_error_name = std::get<0>(error);

	if (character_level_error_name == "")
		return "Empty error tuple -- possible cause: successful or partial match";

	std::string::const_iterator error_position = std::get<1>(error);

	std::string::const_iterator from = std::abs(std::distance(context.first, error_position)) < offset ? context.first : error_position - offset;
	std::string::const_iterator to = std::abs(std::distance(error_position, context.second)) < offset ? context.second : error_position + offset;

	std::string before(from, error_position);
	std::string after(error_position + 1, to);
	std::string space(before.size(), ' ');

	for (size_t i = 0; i < before.size(); ++i)
		if (before[i] == '\t')
			space[i] = '\t';
	
	std::stringstream message;
	message	<< character_level_error_name << " failed while trying to match a(n) " << std::get<2>(error) <<  ":\n" 
			<< before << std::endl << space << *error_position << std::endl
			<< space << " " << after << std::endl 
			<< space << "^";

	return message.str();
}

std::string read_file_to_string(std::string const &filename) {
	std::fstream source(filename, std::ios::in);
	if (!source) throw std::runtime_error(h119::util::format("Couldn't open file: %0", filename));

	source >> std::noskipws;

	source.seekg (0, source.end);
	size_t length = source.tellg();
	source.seekg (0, source.beg);

	std::string the_contents;
	the_contents.reserve(length);
	std::copy(
			std::istream_iterator<char>(source),
			std::istream_iterator<char>(),
			std::inserter(
				the_contents,
				the_contents.end()
				)
			);

	return the_contents;
}

}}
