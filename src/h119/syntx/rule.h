#ifndef H119_SYNTX_RULE
#define H119_SYNTX_RULE

#include <memory>          // std::shared_ptr
#include <exception>       // std::exception

#include <h119/syntx/core_types.h>
#include <h119/syntx/base_rule.h>
#include <h119/util/format.h>

namespace h119 { namespace syntx {

class rule : public base_rule {
	public:
		class unassigned_rule : public std::exception {
			private:
				char const * name;

			public:
				unassigned_rule(char const *name) : name(name) {}

				virtual char const *what() const noexcept override {
					return h119::util::format("Rule %0 has been left unassigned", name).c_str();
				}
		};

	private:
		char const *name;
		std::shared_ptr<std::shared_ptr<base_rule>> the_rule;

	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;

	public:
		rule(char const *name = nullptr, std::shared_ptr<base_rule> a_rule = nullptr) : name(name), the_rule(new std::shared_ptr<base_rule >(a_rule)) {}

		void set_rule(std::shared_ptr <base_rule > a_rule) {
			*the_rule = a_rule;
		}

		rule &operator <<=(base_rule const &a_rule) {
			set_rule(a_rule.clone());
			return *this;
		}
		
		std::shared_ptr<base_rule> clone() const override;

		char const *get_name() const {return name;}
};

bool parse(rule &a_rule, match_range &context, match_range &result, std::shared_ptr<node> &root, syntax_error &error);

}}

#endif // H119_SYNTX_RULE
