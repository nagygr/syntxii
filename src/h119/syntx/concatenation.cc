#include <string>          // std::string

#include <h119/util/logger.h>
#include <h119/syntx/concatenation.h>

namespace h119 { namespace syntx {

bool concatenation::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	match_range local = context, lhs_match, rhs_match;

	if (lhs->match(local, lhs_match, root, error) && rhs->match(local, rhs_match, root, error)) {
		matching_range.first = context.first;
		matching_range.second = rhs_match.second;
		context = local;

		log_3("Concatenation matched (%0)", std::string(matching_range.first, matching_range.second));

		return true;
	}
	
	log_5("Concatenation failed at character %0", *context.first);
	return false;
}

std::shared_ptr<base_rule> concatenation::clone() const {
	return std::make_shared<concatenation>(*this);
}

concatenation operator <<(base_rule const &lhs_rule, base_rule const &rhs_rule) {
	return concatenation(lhs_rule.clone(), rhs_rule.clone());
}

}}
