#include <h119/syntx/if_next_node.h>

namespace h119 { namespace syntx {

bool if_next_node::generate(generator_range &context) {
	if (context.first == context.second) return false;

	std::shared_ptr<node> current_node = *context.first;
	if (current_node->name != name) return false;

	bool result = true;
	for (auto a_generator: the_generators) {
		result = result && a_generator->generate(context);
	}

	return result;
}

std::shared_ptr<base_generator> if_next_node::clone() const {
	return std::make_shared<if_next_node>(*this);
}

}}
