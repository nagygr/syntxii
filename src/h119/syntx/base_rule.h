#ifndef H119_SYNTX_BASERULE
#define H119_SYNTX_BASERULE

#include <utility>         // std::pair
#include <string>          // std::string
#include <memory>          // std::shared_ptr
#include <vector>          // std::vector
#include <tuple>           // std::tuple

#include <h119/syntx/core_types.h>
#include <h119/syntx/node.h>

namespace h119 { namespace syntx {

class base_rule {
	private:
		virtual bool test(match_range &context , match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) = 0;

	public:
		virtual ~base_rule() {}

		bool match(match_range &context , match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error);

		virtual std::shared_ptr<base_rule> clone() const = 0;
};

std::string error_message(match_range const &context, syntax_error const &error);

std::string read_file_to_string(std::string const &filename);

}}

#endif // H119_SYNTX_BASERULE

