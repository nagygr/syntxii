#include <h119/syntx/alternation.h>

namespace h119 { namespace syntx {

bool alternation::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	match_range local = context, lhs_match, rhs_match;

	if (lhs->match(local, lhs_match, root, error)) {
		matching_range.first = context.first;
		matching_range.second = lhs_match.second;
		context = local;

		return true;
	}

	if (rhs->match(local, rhs_match, root, error)) {
		matching_range.first = context.first;
		matching_range.second = rhs_match.second;
		context = local;

		return true;
	}

	
	return false;
}

std::shared_ptr<base_rule> alternation::clone() const {
	return std::make_shared<alternation>(*this);
}

alternation operator |(base_rule const &lhs_rule, base_rule const &rhs_rule) {
	return alternation(lhs_rule.clone(), rhs_rule.clone());
}

}}
