#ifndef H119_SYNTX_REPETITION
#define H119_SYNTX_REPETITION

#include <memory>          // std::shared_ptr

#include <h119/syntx/core_types.h>
#include <h119/syntx/base_rule.h>

namespace h119 { namespace syntx {

class repetition : public base_rule {
	private:
		std::shared_ptr<base_rule> the_rule;

	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;

	public:
		repetition(std::shared_ptr<base_rule> a_rule) : the_rule(a_rule) {}
		virtual std::shared_ptr<base_rule> clone() const override;
};

repetition operator +(base_rule const &a_rule);

}}

#endif // H119_SYNTX_REPETITION
