#ifndef H119_SYNTX_BASICTYPES
#define H119_SYNTX_BASICTYPES

#include <tuple>           // std::tuple
#include <string>          // std::string
#include <utility>         // std::pair

namespace h119 { namespace syntx {

using match_range = std::pair<std::string::const_iterator, std::string::const_iterator>;
using syntax_error = std::tuple<std::string, std::string::const_iterator, std::string>;

}}

#endif // H119_SYNTX_BASICTYPES
