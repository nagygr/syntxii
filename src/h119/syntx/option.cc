#include <utility>          // std::pair

#include <h119/syntx/option.h>

namespace h119 { namespace syntx {

bool option::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	match_range local_context = context, optional_match;

	if (optional->match(local_context, optional_match, root, error)) {
		matching_range = std::make_pair(context.first, local_context.first);
		context = local_context;
	}
	else {
		matching_range = std::make_pair(context.first, context.first);
	}

	
	return true;
}

std::shared_ptr<base_rule> option::clone() const {
	return std::make_shared<option>(*this);
}

option operator !(base_rule const &optional) {
	return option(optional.clone());
}

}}
