#ifndef H119_SYNTX_KEYWORD
#define H119_SYNTX_KEYWORD

#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/syntx/base_rule.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/character_level_rule.h>
#include <h119/syntx/node.h>

namespace h119 { namespace syntx {

class keyword : public base_rule, public character_level_rule {
	private:
		char const *the_word;
		char const *extra_characters;

	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;
		virtual std::string description() const override;

	public:
		keyword(char const *the_word, char const *extra_characters = "_") : 
			the_word(the_word), 
			extra_characters(extra_characters) {}

		keyword(keyword const &other) = default;
		
		virtual std::shared_ptr<base_rule> clone() const override;
};

}}

#endif // H119_SYNTX_KEYWORD


