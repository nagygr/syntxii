#include <memory>          // std::shared_ptr
#include <tuple>           // std::tuple

#include <h119/util/logger.h>
#include <h119/syntx/rule.h>

namespace h119 { namespace syntx {

bool rule::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	match_range local = context, the_match;
	std::shared_ptr<node> rule_node =
		name == nullptr
			? root
			: std::make_shared<node>(name);

	if (*the_rule == nullptr) throw unassigned_rule(name);

	if ((*the_rule)->match(local, the_match, rule_node, error)) {
		matching_range.first = context.first;
		matching_range.second = the_match.second;
		context = local;

		if (name != nullptr) {
			rule_node->set_match_range(matching_range);
			root->children.push_back(rule_node);
		}

		log_3("Rule %1 matched (%0)", std::string(matching_range.first, matching_range.second), name == nullptr ? "<unnamed>" : name);
		
		return true;
	}
	else {
		if (std::get<2>(error) == "") {
			log_5("Rule %0 failed, setting the rule name in the error tuple", name == nullptr ? "<unnamed>" : name);
			std::get<2>(error) = name == nullptr ? "<unnamed>" : name;
		}
	}
	
	log_5("Rule %1 failed at character %0", *context.first, name == nullptr ? "<unnamed>" : name);
	return false;
}

std::shared_ptr<base_rule> rule::clone() const {
	return std::make_shared<rule>(*this);
}

bool parse(rule &a_rule, match_range &context, match_range &result, std::shared_ptr<node> &root, syntax_error &error) {
	bool match = a_rule.match(context, result, root, error);
	bool full_match = context.first == context.second;

	log_5("The parsing of a rule resulted in match: %0, full match: %1", match, full_match);

	if (match && !full_match) {
		error = syntax_error(h119::util::format("Not a full match -- current character: \"%0\" ([position (pointers)] current: %1, end: %2)", *context.first, &context.first, &context.second), context.first, a_rule.get_name());
	}

	return match && full_match;
}

}}
