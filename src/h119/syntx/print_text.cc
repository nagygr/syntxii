#include <h119/syntx/print_text.h>

namespace h119 { namespace syntx {

bool print_text::generate(generator_range &context) {
	(*stream) << text;
	return true;
}

std::shared_ptr<base_generator> print_text::clone() const {
	return std::make_shared<print_text>(*this);
}

}}
