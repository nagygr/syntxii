#ifndef H119_SYNTX_STRING
#define H119_SYNTX_STRING

#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/syntx/base_rule.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/character_level_rule.h>
#include <h119/syntx/node.h>

namespace h119 { namespace syntx {

class string : public base_rule, public character_level_rule {
	private:
		char delimiter;
		char escape_character;
		
	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;
		virtual std::string description() const override;

	public:
		string(char delimiter = '\"', char escape_character = '\\') :
			delimiter(delimiter),
			escape_character(escape_character) {}
		
		virtual std::shared_ptr<base_rule> clone() const override;
};

}}

#endif // H119_SYNTX_STRING

