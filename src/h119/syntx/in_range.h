#ifndef H119_SYNTX_INRANGE
#define H119_SYNTX_INRANGE

#include <memory>          // std::shared_ptr
#include <stdexcept>       // std::invalid_argument
#include <string>          // std::string

#include <h119/syntx/base_rule.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/character_level_rule.h>
#include <h119/syntx/node.h>

namespace h119 { namespace syntx {

class in_range : public base_rule, public character_level_rule {
	private:
		char range_first;
		char range_last;
	
	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;

	public:
		in_range(char range_first, char range_last) :
			range_first(range_first),
			range_last(range_last) {}
		virtual std::shared_ptr<base_rule> clone() const override;
		virtual std::string description() const override;
};

}}

#endif // H119_SYNTX_INRANGE
