#include <h119/syntx/print_value.h>

namespace h119 { namespace syntx {

bool print_value::generate(generator_range &context) {
	if (context.first == context.second) return false;

	std::shared_ptr<node> current_node = *context.first;

	if (current_node->name != name) return false;

	match_range range = current_node->get_match_range();
	(*stream) << std::string(range.first, range.second);

	++context.first;
	return true;
}

std::shared_ptr<base_generator> print_value::clone() const {
	return std::make_shared<print_value>(*this);
}

}}
