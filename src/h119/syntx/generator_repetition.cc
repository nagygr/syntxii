#include <h119/syntx/generator_repetition.h>

namespace h119 { namespace syntx {

bool generator_repetition::generate(generator_range &context) {
	generator_range local = context;

	if (repeated_generator->generate(local)) {
		while (repeated_generator->generate(local));

		context = local;

		return true;
	}

	return false;
}

std::shared_ptr<base_generator> generator_repetition::clone() const {
	return std::make_shared<generator_repetition>(*this);
}

generator_repetition operator +(base_generator const &a_generator) {
	return generator_repetition(a_generator.clone());
}

}}
