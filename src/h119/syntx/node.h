#ifndef H119_SYNTX_NODE
#define H119_SYNTX_NODE

#include <cstddef>         // size_t
#include <memory>          // std::shared_ptr
#include <vector>          // std::vector
#include <ostream>         // std::ostream
#include <iostream>        // std::cout
#include <string>          // std::string

#include <h119/util/logger.h>
#include <h119/syntx/core_types.h>

namespace h119 { namespace syntx {

struct node {
	char const *name;
	std::vector<std::shared_ptr<node>> children;

	using node_iterator = std::vector<std::shared_ptr<node>>::const_iterator;

	node(char const *name, match_range matching_range = match_range()) : name(name), matching_range(matching_range) {}

	void set_match_range(match_range range) {
		matching_range = range;
	}

	match_range get_match_range() {
		/*
		 * This log entry can be used to check the value of the matching_range member. 
		 * //log_5("Node (%0) inner match range: %1", name, std::string(matching_range.first, matching_range.second));
		 */

		/*
		 * The logic below is needed because of the root node. The other nodes will always contain a correct matching range
		 * as every base_rule calculates its match_range recursively, using its children.
		 */
		size_t size = children.size();
		if (size == 0) return matching_range;
		else return match_range(children[0]->matching_range.first, children[size - 1]->matching_range.second);
	}

	node_iterator cbegin() const {
		return children.cbegin();
	}

	node_iterator cend() const {
		return children.cend();
	}
	
	private:
		match_range matching_range;
};

void print_tree(std::shared_ptr<node> root, size_t level = 0);
void create_dot_tree(std::shared_ptr<node> root, std::ostream &stream = std::cout);

}}

#endif // H119_SYNTX_NODE
