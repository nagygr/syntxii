#ifndef H119_SYNTX_PERFORM
#define H119_SYNTX_PERFORM

#include <string>          // std::string
#include <memory>          // std::shared_ptr
#include <functional>      // std::function

#include <h119/syntx/base_generator.h>

namespace h119 { namespace syntx {

class perform : public base_generator {
	private:
		std::string name;
		std::function<bool(generator_range &)> action;

	public:
		perform(std::string const &name, std::function<bool(generator_range&)> action) : name(name), action(action) {}
		virtual bool generate(generator_range &context) override;
		virtual std::shared_ptr<base_generator> clone() const override;
};

}}

#endif // H119_SYNTX_PERFORM


