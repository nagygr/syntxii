#ifndef H119_SYNTX_OPTION
#define H119_SYNTX_OPTION

#include <memory>          // std::shared_ptr

#include <h119/syntx/base_rule.h>
#include <h119/syntx/core_types.h>

namespace h119 { namespace syntx {

class option : public base_rule {
	private:
		std::shared_ptr<base_rule> optional;

	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;

	public:
		option(std::shared_ptr<base_rule> optional) :
			optional(optional) {}
		virtual std::shared_ptr<base_rule> clone() const override;
};

option operator !(base_rule const &optional);

}}

#endif // H119_SYNTX_OPTION

