#include <utility>         // std::pair
#include <cstddef>         // size_t

#include <h119/util/format.h>
#include <h119/util/string.h>
#include <h119/util/logger.h>
#include <h119/syntx/real.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool real::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	match_range local_context = context;

	character_range non_zero{'1', '9'}, numeric{'0', '9'};
	char neg_sign = '-', pos_sign = '+', zero = '0';
	char const *exponent = "eE";
	char decimal = decimal_mark_character[static_cast<size_t>(the_decimal_mark)]; 
	
	bool has_sign = false, has_int_part = false, has_decimal = false;
	bool has_frac_part = false;
	bool has_exponent_sign = false, has_exponent_letter = false;
	bool has_exponent = false;
	bool single_digit_int_part = false;

	if (
		step_if_matches(neg_sign, local_context.first) ||
		step_if_matches(pos_sign, local_context.first)
	) {
		has_sign = true;
	}

	if (*local_context.first == zero) {
		++local_context.first;
		has_int_part = true;
		single_digit_int_part = true;
	}
	else {
		if (in_range(*local_context.first, non_zero)) {
			++local_context.first;
			has_int_part = true;

			while (in_range(*local_context.first, numeric)) {
				++local_context.first;
			}
		}
	}

	if (*local_context.first == decimal) {
		++local_context.first;
		has_decimal = true;
	}

	if (in_range(*local_context.first, numeric)) {
		++local_context.first;
		has_frac_part = true;

		while (in_range(*local_context.first, numeric)) {
			++local_context.first;
		}
	}

	if (in_set(*local_context.first, exponent)) {
		++local_context.first;
		has_exponent_letter = true;

		if (
			step_if_matches(neg_sign, local_context.first) ||
			step_if_matches(pos_sign, local_context.first)
		) {
			has_exponent_sign = true;
		}

		if (in_range(*local_context.first, numeric)) {
			++local_context.first;
			has_exponent = true;

			while (in_range(*local_context.first, numeric)) {
				++local_context.first;
			}
		}
	}
	
	/*
	 * The 
	 *  (has_exponent_letter && has_exponent_sign && !has_exponent) ||
	 *  (has_exponent_letter && !has_exponent_sign && !has_exponent)
	 * expression could be simplified by elminating has_exponent_sign but
	 * it is used like this intentionally to draw attention to both 
	 * error cases.
	 */
	if (
		(local_context.first == context.first) ||
		(has_sign && !has_int_part && !has_decimal && !has_exponent_letter) ||
		(single_digit_int_part && !has_decimal && has_frac_part) ||
		(has_sign && !has_int_part && !has_frac_part && has_exponent_letter) ||
		(has_exponent_letter && has_exponent_sign && !has_exponent) ||
		(has_exponent_letter && !has_exponent_sign && !has_exponent)
	) {
		log_5("Failed to match real at %0", *context.first);
		set_error_message(context.first, error);
		return false;
	}

	matching_range = std::make_pair(context.first, local_context.first);
	context = local_context;

	log_3("Real (decimal mark: %0) matched (%1)", the_decimal_mark == decimal_mark::point ? "point" : "comma", std::string(matching_range.first, matching_range.second));

	return true;
}

std::shared_ptr<base_rule> real::clone() const {
	return std::make_shared<real>(*this);
}

std::string real::description() const {
	return "A real";
}

}}

