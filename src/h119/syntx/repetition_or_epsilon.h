#ifndef H119_SYNTX_REPETITIONOREPSILON
#define H119_SYNTX_REPETITIONOREPSILON

#include <memory>          // std::shared_ptr

#include <h119/syntx/core_types.h>
#include <h119/syntx/base_rule.h>

namespace h119 { namespace syntx {

class repetition_or_epsilon : public base_rule {
	private:
		std::shared_ptr<base_rule> the_rule;

	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;

	public:
		repetition_or_epsilon(std::shared_ptr<base_rule> a_rule) : the_rule(a_rule) {}
		virtual std::shared_ptr<base_rule> clone() const override;
};

repetition_or_epsilon operator *(base_rule const &a_rule);

}}

#endif // H119_SYNTX_REPETITIONOREPSILON
