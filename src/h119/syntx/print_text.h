#ifndef H119_SYNTX_PRINTTEXT
#define H119_SYNTX_PRINTTEXT

#include <memory>          // std::shared_ptr
#include <ostream>         // std::ostream
#include <iostream>        // std::cout

#include <h119/syntx/base_generator.h>

namespace h119 { namespace syntx {

class print_text : public base_generator {
	private:
		std::string text;
		std::ostream *stream;

	public:
		print_text(std::string const &text, std::ostream &stream = std::cout) : text(text), stream(&stream) {}
		virtual bool generate(generator_range &context) override;
		virtual std::shared_ptr<base_generator> clone() const override;
};

}}

#endif // H119_SYNTX_PRINTTEXT 
