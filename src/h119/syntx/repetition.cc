#include <string>          // std::string
#include <utility>         // std::pair

#include <h119/util/logger.h>
#include <h119/syntx/repetition.h>

namespace h119 { namespace syntx {

bool repetition::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	match_range local = context, the_match;

	if (the_rule->match(local, the_match, root, error)) {

		while (the_rule->match(local, the_match, root, error)) {
		}
	
		matching_range = std::make_pair(context.first, the_match.second);
		context = local;
		
		log_3("Repetition matched (%0)", std::string(matching_range.first, matching_range.second));

		return true;
	}
	
	log_5("Repetition failed at character %0", *context.first);
	return false;
}

std::shared_ptr<base_rule> repetition::clone() const {
	return std::make_shared<repetition>(*this);
}

repetition operator +(base_rule const &a_rule) {
	return repetition(a_rule.clone());
}

}}
