#ifndef H119_SYNTX_CONCATENATION
#define H119_SYNTX_CONCATENATION

#include <memory>          // std::shared_ptr

#include <h119/syntx/core_types.h>
#include <h119/syntx/base_rule.h>

namespace h119 { namespace syntx {

class concatenation : public base_rule {
	private:
		std::shared_ptr<base_rule> lhs;
		std::shared_ptr<base_rule> rhs;

	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;

	public:
		concatenation(std::shared_ptr<base_rule> lhs, std::shared_ptr<base_rule> rhs) : lhs(lhs), rhs(rhs) {}
		virtual std::shared_ptr<base_rule> clone() const override;
};

concatenation operator <<(base_rule const &lhs_rule, base_rule const &rhs_rule);

}}

#endif // H119_SYNTX_CONCATENATION

