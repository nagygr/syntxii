#ifndef H119_SYNTX_CHARACTERLEVELRULE
#define H119_SYNTX_CHARACTERLEVELRULE

#include <string>          // std::string
#include <tuple>           // std::tuple

#include <h119/syntx/core_types.h>
#include <h119/util/logger.h>

namespace h119 { namespace syntx {

class character_level_rule {
	private:
		virtual std::string description() const = 0;

	protected:
		void set_error_message(std::string::const_iterator current_position, syntax_error &error) {
			if (current_position > std::get<1>(error) || std::get<0>(error) == "") {
				log_5("Setting the syntax error position at: %0", *current_position);
				error = syntax_error(description(), current_position, "");
			}
		}

	public:
		virtual ~character_level_rule() {};
};

}}

#endif // H119_SYNTX_CHARACTERLEVELRULE
