#include <h119/syntx/generator_repetition_or_epsilon.h>

namespace h119 { namespace syntx {

bool generator_repetition_or_epsilon::generate(generator_range &context) {
	generator_range local = context;
	bool there_was_a_match = false;

	while (repeated_generator->generate(local)) {
		there_was_a_match = true;
	}

	if (there_was_a_match) {
		context = local;
	}

	return true;
}

std::shared_ptr<base_generator> generator_repetition_or_epsilon::clone() const {
	return std::make_shared<generator_repetition_or_epsilon>(*this);
}

generator_repetition_or_epsilon operator *(base_generator const &a_generator) {
	return generator_repetition_or_epsilon(a_generator.clone());
}

}}
