#include <utility>         // std::pair

#include <h119/util/format.h>
#include <h119/util/string.h>
#include <h119/util/logger.h>
#include <h119/syntx/identifier.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool identifier::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	match_range local_context = context;

	character_range lower{'a', 'z'}, upper{'A', 'Z'}, numeric{'0', '9'};

	if (
		in_range(*local_context.first, lower) ||
		in_range(*local_context.first, upper) ||
		in_set(*local_context.first, extra_characters)
	) {
		++local_context.first;

		while (
			in_range(*local_context.first, lower) ||
			in_range(*local_context.first, upper) ||
			in_range(*local_context.first, numeric) ||
			in_set(*local_context.first, extra_characters)
		)
			++local_context.first;

		matching_range = std::make_pair(context.first, local_context.first);
		context = local_context;

		log_3("Identifier matched (%0)", std::string(matching_range.first, matching_range.second));
		return true;
	}

	log_5("Failed to match identifier at %0", *context.first);
	set_error_message(context.first, error);
	return false;
}

std::shared_ptr<base_rule> identifier::clone() const {
	return std::make_shared<identifier>(*this);
}

std::string identifier::description() const {
	return format("An identifier (extra characters: \"%0\")", extra_characters);
}

}}

