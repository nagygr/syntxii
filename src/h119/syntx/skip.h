#ifndef H119_SYNTX_SKIP
#define H119_SYNTX_SKIP

#include <string>          // std::string
#include <ostream>         // std::ostream
#include <iostream>        // std::cout
#include <memory>          // std::shared_ptr

#include <h119/syntx/base_generator.h>

namespace h119 { namespace syntx {

class skip : public base_generator {
	private:
		std::string name;

	public:
		skip(std::string const &name) : name(name) {}
		virtual bool generate(generator_range &context) override;
		virtual std::shared_ptr<base_generator> clone() const override;

};

}}

#endif // H119_SYNTX_SKIP


