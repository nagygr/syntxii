#include <h119/util/format.h>
#include <h119/util/string.h>
#include <h119/util/logger.h>
#include <h119/syntx/not_in_range.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool not_in_range::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	if (!(range_first <= *context.first && *context.first <= range_last)) {
		matching_range = std::make_pair(context.first , context.first + 1);
		++context.first;

		log_3("A character not in a given range matched (%0)", *matching_range.first);

		return true;
	}

	log_5("Failed to match a character not in the given range at %0", *context.first);
	set_error_message(context.first, error);
	return false;
}

std::shared_ptr<base_rule> not_in_range::clone() const {
	return std::make_shared<not_in_range>(*this);
}

std::string not_in_range::description() const {
	return format("A character not in the range: [\'%0\', \'%1\']", range_first, range_last);
}

}}
