#ifndef H119_SYNTX_REAL
#define H119_SYNTX_REAL

#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/syntx/base_rule.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/character_level_rule.h>
#include <h119/syntx/node.h>

namespace h119 { namespace syntx {

class real : public base_rule, public character_level_rule {
	public:
		enum class decimal_mark {
			point,
			comma
		};

		char const *decimal_mark_character = ".,";

	private:
		decimal_mark the_decimal_mark;

	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;
		virtual std::string description() const override;

	public:
		real(decimal_mark the_decimal_mark = decimal_mark::point) :
			the_decimal_mark(the_decimal_mark) {}
		virtual std::shared_ptr<base_rule> clone() const override;
};

}}

#endif // H119_SYNTX_REAL

