#ifndef H119_SYNTX_WHITESPACE
#define H119_SYNTX_WHITESPACE

#include <memory>          // std::shared_ptr
#include <string>          // std::string

#include <h119/syntx/base_rule.h>
#include <h119/syntx/core_types.h>
#include <h119/syntx/character_level_rule.h>
#include <h119/syntx/node.h>

namespace h119 { namespace syntx {

class whitespace : public base_rule {
	private:
		std::shared_ptr<base_rule> the_rule;
	
	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;

	public:
		whitespace(std::shared_ptr<base_rule> the_rule) : the_rule(the_rule) {}
		virtual std::shared_ptr<base_rule> clone() const override;
};

whitespace operator -(base_rule const &a_rule);

}}

#endif // H119_SYNTX_WHITESPACE


