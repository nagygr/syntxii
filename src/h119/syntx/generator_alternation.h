#ifndef H119_SYNTX_GENERATORALTERNATION
#define H119_SYNTX_GENERATORALTERNATION

#include <memory>          // std::shared_ptr

#include <h119/syntx/base_generator.h>

namespace h119 { namespace syntx {

class generator_alternation : public base_generator {
	private:
		std::shared_ptr<base_generator> lhs;
		std::shared_ptr<base_generator> rhs;

	public:
		generator_alternation(std::shared_ptr<base_generator> lhs, std::shared_ptr<base_generator> rhs) : lhs(lhs), rhs(rhs) {}
		virtual bool generate(generator_range &context) override;
		virtual std::shared_ptr<base_generator> clone() const override;
};

generator_alternation operator |(base_generator const &lhs, base_generator const &rhs);

}}

#endif // H119_SYNTX_GENERATORALTERNATION

