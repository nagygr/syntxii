#ifndef H119_SYNTX_NONGREEDYREPETITIONOREPSILON
#define H119_SYNTX_NONGREEDYREPETITIONOREPSILON

#include <memory>          // std::shared_ptr

#include <h119/syntx/core_types.h>
#include <h119/syntx/base_rule.h>

namespace h119 { namespace syntx {

class non_greedy_repetition_or_epsilon : public base_rule {
	private:
		std::shared_ptr<base_rule> repeated_rule;
		std::shared_ptr<base_rule> following_rule;

	private:
		virtual bool test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) override;

	public:
		non_greedy_repetition_or_epsilon(
			std::shared_ptr<base_rule> repeated_rule,
			std::shared_ptr<base_rule> following_rule
		) :
			repeated_rule(repeated_rule),
			following_rule(following_rule)
		{}
		virtual std::shared_ptr<base_rule> clone() const override;
};

non_greedy_repetition_or_epsilon operator *(
	base_rule const &repeated_rule,
	base_rule const &following_rule
);

}}

#endif // H119_SYNTX_NONGREEDYREPETITIONOREPSILON
