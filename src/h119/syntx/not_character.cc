#include <h119/util/format.h>
#include <h119/util/string.h>
#include <h119/util/logger.h>
#include <h119/syntx/not_character.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool not_character::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	if (!in_set(*context.first, disallowed_characters)) {
		matching_range = std::make_pair(context.first , context.first + 1);
		++context.first;

		log_3("A character not in a given set matched (%0)", *context.first);

		return true;
	}

	log_5("Failed to match a character not in the given set at %0", *context.first);
	set_error_message(context.first, error);
	return false;
}

std::shared_ptr<base_rule> not_character::clone() const {
	return std::make_shared<not_character>(*this);
}

std::string not_character::description() const {
	return format("A character not in the character set: \"%0\"", disallowed_characters);
}

}}
