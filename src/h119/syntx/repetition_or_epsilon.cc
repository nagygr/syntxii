#include <string>          // std::string

#include <h119/util/logger.h>
#include <h119/syntx/repetition_or_epsilon.h>

namespace h119 { namespace syntx {

bool repetition_or_epsilon::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	match_range local = context, the_match;
	matching_range.first = context.first;
	matching_range.second = context.first;

	while (the_rule->match(local, the_match, root, error)) {
		matching_range.second = the_match.second;
		context = local;
	}

	log_3("Repetition or epsilon matched (%0)", std::string(matching_range.first, matching_range.second));

	return true;
}

std::shared_ptr<base_rule> repetition_or_epsilon::clone() const {
	return std::make_shared<repetition_or_epsilon>(*this);
}

repetition_or_epsilon operator *(base_rule const &a_rule) {
	return repetition_or_epsilon(a_rule.clone());
}

}}
