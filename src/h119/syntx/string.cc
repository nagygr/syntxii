#include <utility>         // std::pair

#include <h119/util/format.h>
#include <h119/util/logger.h>
#include <h119/syntx/string.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool string::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	match_range local_context = context;

	if (*local_context.first == delimiter) {
		++local_context.first;

		while (local_context.first != local_context.second && *local_context.first != delimiter) {
			if (*local_context.first == escape_character) ++local_context.first;
			++local_context.first;
		}

	}
	
	if (*local_context.first == delimiter) {

		++local_context.first;
		
		matching_range = std::make_pair(context.first, local_context.first);
		context = local_context;

		log_3("String matched (%0)", std::string(matching_range.first, matching_range.second));
		return true;
	}

	log_5("Failed to match string at %0", *context.first);
	set_error_message(context.first, error);
	return false;
}

std::shared_ptr<base_rule> string::clone() const {
	return std::make_shared<string>(*this);
}

std::string string::description() const {
	return format("A string (delimiter: \"%0\", escape character: \"%1\")", delimiter, escape_character);
}

}}

