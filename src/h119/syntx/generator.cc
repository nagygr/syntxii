#include <h119/syntx/generator.h>

namespace h119 { namespace syntx {

std::shared_ptr<base_generator> generator::clone() const {
	return std::make_shared<generator>(*this);
}

bool generator::generate(generator_range &context) {
	if (context.first == context.second) return false;

	std::shared_ptr<node> current_node = *context.first;
	if (current_node->name != name) return false;

	generator_range children(current_node->cbegin(), current_node->cend());

	if ((*the_generator)->generate(children)) {
		++context.first;
		return true;
	}

	return false;
}

}}
