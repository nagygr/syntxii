#include <h119/util/format.h>
#include <h119/util/string.h>
#include <h119/util/logger.h>
#include <h119/syntx/character.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool character::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	if (in_set(*context.first, allowed_characters)) {
		matching_range = std::make_pair(context.first , context.first + 1);
		++context.first;

		log_3("Character matched (%0)", *matching_range.first);

		return true;
	}

	log_5("Failed to match character at %0", *context.first);
	set_error_message(context.first, error);
	return false;
}

std::shared_ptr<base_rule> character::clone() const {
	return std::make_shared<character>(*this);
}

std::string character::description() const {
	return format("A character from the character set: \"%0\"", allowed_characters);
}

}}
