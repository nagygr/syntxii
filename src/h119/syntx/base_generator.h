#ifndef H119_SYNTX_BASEGENERATOR
#define H119_SYNTX_BASEGENERATOR

#include <utility>         // std::pair
#include <memory>          // std::shared_ptr

#include <h119/syntx/node.h>

namespace h119 { namespace syntx {

using generator_range = std::pair<node::node_iterator, node::node_iterator>;

class base_generator {
	public:
		virtual bool generate(generator_range &context) = 0;
		virtual std::shared_ptr<base_generator> clone() const = 0;
		virtual ~base_generator() {}
};

}}

#endif // H119_SYNTX_BASEGENERATOR
