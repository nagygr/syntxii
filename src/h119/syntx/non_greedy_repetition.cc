#include <string>          // std::string

#include <h119/util/logger.h>

#include <h119/syntx/non_greedy_repetition.h>

namespace h119 { namespace syntx {

bool non_greedy_repetition::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	match_range local_context = context, the_match;
	matching_range.first = context.first;
	matching_range.second = context.first;

	// The repeated rule has to match at least once here
	if (repeated_rule->match(local_context, the_match, root, error)) {

		bool following_matched = false;
		if (following_rule->match(local_context, the_match, root, error)) {
			following_matched = true;
		}
		else {
			while (
				!following_matched &&
				repeated_rule->match(local_context, the_match, root, error)
			) {
				if (following_rule->match(local_context, the_match, root, error)) {
					following_matched = true;
				}
			}
		}

		if (following_matched) {
			matching_range = std::make_pair(context.first, local_context.first); 
			context = local_context;

			log_3("Non greedy repetition matched (%0)", std::string(matching_range.first, matching_range.second));

			return true;
		}
	}

	log_5("Failed to match non greedy repetition at %0", *context.first);
	return false;

}

std::shared_ptr<base_rule> non_greedy_repetition::clone() const {
	return std::make_shared<non_greedy_repetition>(*this);
}

non_greedy_repetition operator +(
	base_rule const &repeated_rule,
	base_rule const &following_rule
) {
	return non_greedy_repetition(
		repeated_rule.clone(),
		following_rule.clone()
	);
}

}}
