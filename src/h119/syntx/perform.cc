#include <h119/syntx/perform.h>

namespace h119 { namespace syntx {

bool perform::generate(generator_range &context) {
	if (context.first == context.second) return false;

	std::shared_ptr<node> current_node = *context.first;
	if (current_node->name != name) return false;

	return action(context);
}

std::shared_ptr<base_generator> perform::clone() const {
	return std::make_shared<perform>(*this);
}

}}
