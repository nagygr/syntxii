#include <utility>         // std::pair
#include <cctype>          // std::isspace

#include <h119/util/format.h>
#include <h119/util/logger.h>
#include <h119/syntx/whitespace.h>

namespace h119 { namespace syntx {

using namespace h119::util;

bool whitespace::test(match_range &context, match_range &matching_range, std::shared_ptr<node> &root, syntax_error &error) {
	if (context.first == context.second) return false;

	match_range local_context = context, local_matching_range;

	while (
		local_context.first != local_context.second &&
		std::isspace(*local_context.first)
	) {
		++local_context.first;
	}

	if (the_rule->match(local_context, local_matching_range, root, error)) {
		matching_range = std::make_pair(context.first, local_context.first);
		context = local_context;

		return true;
	}

	return false;
}

std::shared_ptr<base_rule> whitespace::clone() const {
	return std::make_shared<whitespace>(*this);
}

whitespace operator -(base_rule const &a_rule) {
	return whitespace(a_rule.clone());
}

}}


