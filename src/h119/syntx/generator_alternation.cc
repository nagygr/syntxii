#include <h119/syntx/generator_alternation.h>

namespace h119 { namespace syntx {

bool generator_alternation::generate(generator_range &context) {
	generator_range local = context;

	if (lhs->generate(local)) {
		context = local;
		return true;
	}

	local = context;
	if (rhs->generate(local)) {
		context = local;
		return true;
	}

	return false;
}

std::shared_ptr<base_generator> generator_alternation::clone() const {
	return std::make_shared<generator_alternation>(*this);
}

generator_alternation operator |(base_generator const &lhs, base_generator const &rhs) {
	return generator_alternation(lhs.clone(), rhs.clone());
}

}}
