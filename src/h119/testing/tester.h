#ifndef H119_TESTING_TESTER
#define H119_TESTING_TESTER 

#include <cstddef>         // size_t
#include <tuple>           // std::tuple
#include <string>          // std::string
#include <vector>          // std::vector
#include <functional>      // std::function

namespace h119 { namespace testing {

#define start_testcase(name) static h119::testing::tester name(__FILE__, __LINE__, #name, []() -> bool {
#define end_testcase });

class tester {
	private:
		static std::vector<std::tuple<std::string, size_t, std::string, std::function<bool()>>> testcases;
		static void add(std::tuple<std::string, size_t, std::string, std::function<bool()>> const &test);
		static size_t test_number();

	public:
		tester(std::string const &filename, size_t line_number, std::string const &test_case_name, std::function<bool()> test_function);

		static bool run_tests();
};

}}

#endif // H119_TESTING_TESTER
