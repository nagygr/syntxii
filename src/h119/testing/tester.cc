#include <iostream>        // std::cout
#include <vector>          // std::vector
#include <string>          // std::string
#include <tuple>           // std::tuple

#include <h119/testing/tester.h>
#include <h119/util/format.h>

namespace h119 { namespace testing {

std::vector<std::tuple<std::string, size_t, std::string, std::function<bool()>>> tester::testcases;

tester::tester(std::string const &filename, size_t line_number, std::string const &test_case_name, std::function<bool()> test_function) {
	tester::add(std::make_tuple(filename, line_number, test_case_name, test_function));
}

void tester::add(std::tuple<std::string, size_t, std::string, std::function<bool()>> const &test) {testcases.push_back(test);}

size_t tester::test_number() {return testcases.size();}

bool tester::run_tests() {
	using namespace h119::util;

	std::vector<std::tuple<std::string, size_t, std::string>> failed_tests;

	char const *stars = "************************************************************";
	char const *double_line = "============================================================";
	size_t number_of_tests = test_number();
	std::cerr << format("\n%0\n[[ RUNNING TESTS ]]\n%0\nNumber of tests: %1\n%0\n\n", double_line, number_of_tests);

	for (auto &testcase: testcases) { 
		bool this_test_passed;
		std::cerr << format("\n%0\n[Running test: %1 (%2:%3)]\n%0\n", stars, std::get<2>(testcase), std::get<0>(testcase), std::get<1>(testcase));
		if (!(this_test_passed = std::get<3>(testcase)()))
			failed_tests.push_back(std::make_tuple(std::get<0>(testcase), std::get<1>(testcase), std::get<2>(testcase)));
		
		std::cerr << format("\n%0\n[%2 (%1)]\n%0\n", stars, std::get<2>(testcase), this_test_passed ? "PASSED" : "FAILED");
	}

	size_t failed_test_number = failed_tests.size();
	std::cerr << format("\n%0\n[[ SUMMARY: %1/%2 tests passed ]]\n%0\n", double_line, number_of_tests - failed_test_number, number_of_tests);

	if (failed_test_number > 0) {
		std::cerr << "The failed tests were:" << std::endl;

		for (auto &failed_test: failed_tests) {
			std::cerr << format("\t%0 (%1:%2)\n", std::get<2>(failed_test), std::get<0>(failed_test), std::get<1>(failed_test));
		}

		std::cerr << format("\n%0\n", double_line);
	}

	return failed_test_number == 0;
}

}}
