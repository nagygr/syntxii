#include <h119/util/string.h>

namespace h119 { namespace util {

	bool in_range(char a_character, char lower, char upper) {
		return (lower <= a_character && a_character <= upper); 
	}

	bool in_range(char a_character, character_range const &range) {
		return (range.first <= a_character && a_character <= range.second); 
	}

	bool in_set(char a_character, char const *character_set) {
		for (char const *c = character_set; *c != '\0'; ++c) {
			if (*c == a_character) return true;
		}

		return false;
	}

	bool step_if_matches(char a_character, std::string::const_iterator &iterator) {
		if (*iterator == a_character) {
			++iterator;
			return true;
		}

		return false;
	}
}}
