#ifndef H119_UTIL_STRING
#define H119_UTIL_STRING

#include <utility>         // std::pair
#include <string>          // std::string

namespace h119 { namespace util {

using character_range = std::pair<char, char>;

bool in_range(char a_character, char lower, char upper);
bool in_range(char a_character, character_range const &range);
bool in_set(char a_character, char const *character_set);
bool step_if_matches(char a_character, std::string::const_iterator &iterator);

}}

#endif // H119_UTIL_STRING
