% The MIT License (MIT)
% 
% Copyright (c) 2013, Gergely Nagy
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in
% all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
\LoadClass[12pt]{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{h119_doc}[2017/03/04 NNG document style]

\RequirePackage[paper=a4paper, top=4cm, left=2cm, right=2cm, bottom=3cm, headheight=100pt, footskip=40pt]{geometry}
\RequirePackage{fancyhdr} % must be loaded after geometry!
\RequirePackage[utf8]{inputenc}
\RequirePackage[english]{babel}
\RequirePackage{graphicx}
\RequirePackage[pdfborder={0 0 0}]{hyperref}
\RequirePackage[T1]{fontenc}
\RequirePackage{listings} % for better code listing
\RequirePackage{bold-extra} % needed by listings
\RequirePackage[usenames,dvipsnames]{color}
%\RequirePackage[lighttt]{lmodern} % Light typewriter font for LModern
%\RequirePackage{hfoldsty} % old style numbers
\RequirePackage[nodayofweek]{datetime}
\RequirePackage{caption}
\RequirePackage{enumitem}
\RequirePackage{longtable}
\RequirePackage{colortbl}
\RequirePackage{amssymb} % checkbox (checked: $\boxtimes$ -- unchecked: $\square$)
\RequirePackage{url} % defines the command \path which can be used to typeset long paths and anything long in tt
\RequirePackage{changepage}
\RequirePackage[percent]{overpic}
\RequirePackage{wrapfig}
\RequirePackage{tikz}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage[tikz]{mdframed}
%\RequirePackage{sansmath}
\RequirePackage[breakable]{tcolorbox}
\RequirePackage{xcolor}
\RequirePackage{sectsty}
\RequirePackage{pifont}
\RequirePackage{lipsum}
\RequirePackage{caption}

%\renewcommand*\rmdefault{iwona}
%\RequirePackage{libertine}
%\RequirePackage[sfmath]{kpfonts}
\RequirePackage{kpfonts}
%\renewcommand*\rmdefault{\sfdefault}

\RequirePackage{needspace}

\definecolor{headerblue}{RGB}{0,117,192}
\definecolor{darkgray}{RGB}{120,120,120}
\definecolor{lightgray}{RGB}{240,240,240}
\definecolor{h119blue}{RGB}{0,117,192}
\definecolor{h119green}{RGB}{1,208,144}

\hypersetup{colorlinks, breaklinks, urlcolor=h119blue, linkcolor=h119blue}

\captionsetup[figure]{labelfont={bf,color=h119blue},textfont={color=h119blue}}
\captionsetup[table]{labelfont={bf,color=h119blue},textfont={it,color=h119blue}}

%\chapterfont{\color{h119green}}  % sets colour of chapters
\sectionfont{\color{h119green}}
\subsectionfont{\color{h119blue}}
\subsubsectionfont{\color{h119blue}}

%\sansmath

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\fancyheadoffset{\paperwidth}
\fancyhf[HC]{
	\begin{tikzpicture}[remember picture, overlay]
	  \node [anchor=north west, inner sep=0pt]  at (current page.north west)
		 {\includegraphics[width=1.01\paperwidth]{page_header}};
	  \node [anchor=north west, inner sep=20pt]  at (current page.north west)
		 {\normalsize\textrm{\textbf{\textcolor{white}{\rightmark}}}};
	\end{tikzpicture}
}

\fancyhf[FC] {
		\begin{tikzpicture}[remember picture, overlay]
	  		\node [anchor=south west, inner sep=0pt]  at (current page.south west)
				{\includegraphics[width=1.01\paperwidth]{page_footer}};
	  		\node [anchor=south west, inner sep=20pt]  at (current page.south west)
				{
					\begin{tabular}{p{0.5\paperwidth}p{0.5\paperwidth}}
						\footnotesize\textrm{\textbf{\textcolor{white}{https://gitlab.com/nagygr/syntxii}}} &
						\begin{minipage}{0.38\paperwidth}
							\begin{flushright}
								\footnotesize\textrm{\textbf{\textcolor{white}{\thepage}}}
							\end{flushright}
						\end{minipage} \\
					\end{tabular}
				};
		\end{tikzpicture}
}

\newcommand{\sectiontop}[1]{
	\fancyhf[HC]{
		\begin{tikzpicture}[remember picture, overlay]
		  \node [anchor=north west, inner sep=0pt]  at (current page.north west)
			 {\includegraphics[width=1.01\paperwidth]{page_header}};
		  \node [anchor=north west, inner sep=20pt]  at (current page.north west)
			 {\normalsize\textrm{\textbf{\textcolor{white}{#1}}}};
		\end{tikzpicture}
	}
}

\newcommand{\nngtitle}{}
\newcommand{\nngsubtitle}{}
\newcommand{\nngfrontfigure}{}

\newcommand{\nngsettitle}[1]{\renewcommand{\nngtitle}{#1\sectiontop{#1}}}
\newcommand{\nngsetsubtitle}[1]{\renewcommand{\nngsubtitle}{#1}}
\newcommand{\nngsetfrontfigure}[1]{\renewcommand{\nngfrontfigure}{#1}}

\fancypagestyle{coverpagestyle}{
\fancyhf{}
}

\newcommand{\coverpage}{
	\thispagestyle{coverpagestyle}
	\begin{tikzpicture}[remember picture, overlay]
	  \node [anchor=north west, inner sep=0pt]  at (current page.north west)
		 {\includegraphics[width=1.01\paperwidth]{cover_header}};
	\end{tikzpicture}
	\vspace*{32ex}\\
	{\fontsize{1.6cm}{1.8cm}\textrm{\textbf{\textsc{\textcolor{headerblue}{\nngtitle}}}}}\\
	\vspace{1ex}\\
	{\textrm{\Large\textcolor{h119green}{\nngsubtitle}}}
	\begin{tikzpicture}[remember picture, overlay]
	  \node [anchor=south west, inner sep=0pt]  at (current page.south west)
		 {\includegraphics[width=1.01\paperwidth]{cover_footer}};
	  \node [anchor=south east, inner sep=55pt]  at (current page.south east)
		 {\fontsize{1.4cm}{1.6cm}\textrm{\textbf{\textcolor{white}{[h119]~~}}}};
	\end{tikzpicture}
	\newpage
	\setcounter{page}{1}
}

\newcommand{\backcoverpage}{
	\newpage
	\thispagestyle{coverpagestyle}
	\begin{tikzpicture}[remember picture, overlay]
	  \node [anchor=north west, inner sep=0pt]  at (current page.north west)
		 {\includegraphics[width=1.01\paperwidth]{back_cover}};
	  \node [anchor=south east, inner sep=55pt]  at (current page.south east)
		 {
			\begin{minipage}[t]{\textwidth}
				\begin{center}
					\fontsize{0.4cm}{0.6cm}
					\textrm{{\textcolor{white}{Please consider the environment before printing this document.}}}
					\vspace{3em}
					~
				\end{center}
			\end{minipage}
		 };
	\end{tikzpicture}
}

\makeatletter
\renewcommand\tableofcontents{%
    \@starttoc{toc}%
}
\makeatother

\newcommand{\hcontents}{
	\setcounter{tocdepth}{3}
	\hsec{Contents}{contents}
	\hypersetup{colorlinks, breaklinks, urlcolor=h119blue, linkcolor=black}
	\tableofcontents
}


\lstset{
	basicstyle=\footnotesize\rmfamily\color{black}, stringstyle=\rmfamily,
	keywordstyle=\bfseries\rmfamily\color{black},aboveskip=10pt, commentstyle=\rmfamily\color{darkgray},
	belowskip=10pt, tabsize=2, xleftmargin=5pt, captionpos=b, breaklines=true, 
	literate={á}{{\'a}}1 {é}{{\'e}}1 {ó}{{\'o}}1 {õ}{{\"o}}1 {ú}{{\'u}}1 {û}{{\"u}}1
}

\lstdefinelanguage{svn} {
	morekeywords={add, blame, checkout, co, commit, copy, delete, diff, log, merge, resolve, revert, svn,
	status, switch, up, update},
	sensitive=true,
	morestring=[b]"%,
}

\lstdefinelanguage{systxt} {
	morekeywords={gps, source, filename, first_nmea_message_in_cycle, last_nmea_message_in_cycle, interface,
	show_glonass, show_fps, log_fps, show_performance, device, profile_gps, rawdisplay, driver, highres,
	screen_x, screen_y, other, radnav, dummy_locale},
	sensitive=true,
	comment=[l];,
	morestring=[b]"%,
}

\lstdefinelanguage{adbshell} {
	morekeywords={adb, logcat, tee, shell, ls, install, am, push, broadcast},
	sensitive=true,
	morestring=[b]"%,
}

\lstdefinelanguage{android} {
	morekeywords={ant, debug, clean, project, update, create, list, targets},
	sensitive=true,
	morestring=[b]"%,
}

\newcounter{hlisting}
\setcounter{hlisting}{0}

\newenvironment{hcode}[2]{
	%\addtocounter{hlisting}{1}\label{lst:#2}
	\needspace{15ex}
	\refstepcounter{hlisting}\label{lst:#2}
	\begin{center}
	\begin{tcolorbox}[width=0.9\textwidth, breakable, colback=lightgray, colframe=white]
		\begin{center}
			\textcolor{h119blue}{\textbf{{Listing~\arabic{hlisting}.}}~~\textit{#1}}
		\end{center}
}
{
	\end{tcolorbox}
	\end{center}
}

\newcommand{\hcoderef}[1]{\emph{Listing\,\ref{lst:#1}}.\/}

\newcommand{\enquote}[1]{``#1''}
\newcommand{\idez}[1]{,,#1''}
\newcommand{\todo}[1]{\textcolor{red}{\textbf{$[$TODO: #1$]$}}}

\newcommand{\hsec}[2]{
	\clearpage
	\newpage
	\section{#1}
	\label{sec:#2}
	\sectiontop{#1}
}

\newcommand{\hsubsec}[2]{
	\subsection{#1}
	\label{subsec:#2}
}

\newcommand{\hsubsubsec}[2]{
	\subsubsection{#1}
	\label{subsubsec:#2}
}

\newcommand{\hparagraph}[2]{
	\paragraph{#1}
	\label{paragraph:#2}
}

\newcommand{\hnameref}[1]{\ding{43}\,\ref{#1}~\nameref{#1} starting on page\,\pageref{#1}}

\newcommand{\hnameonlyref}[1]{\ding{43}\,\nameref{#1} starting on page\,\pageref{#1}}

\newenvironment{hcomment}
{
	\begin{center}
		\begin{minipage}[h]{0.8\linewidth}
			\footnotesize\itshape
			\begin{tabular}[t]{lp{0.8\linewidth}}
			%\textcolor{h119green}{$\clubsuit$} &
			\textcolor{h119green}{\textsc{Comment}} &
}
{
			\end{tabular}
		\end{minipage}
	\end{center}
}

\newenvironment{habstract}
{
	\vspace*{1ex}
	\begin{center}
		\begin{minipage}[h]{0.9\linewidth}
			\setlength{\parskip}{1em}
			\itshape
}
{
		\end{minipage}
	\end{center}
	\vspace{5ex}
}

\newenvironment{htechdetails}[1]
{
\begin{adjustwidth}{2.2em}{2.2em}
\footnotesize
\setlength{\parskip}{0.8em}
\textcolor{h119blue}{$\blacksquare$~~\textit{\textbf{#1}}}
\vspace{0.5em}\\
}
{
\end{adjustwidth}
}

\newcommand{\htodo}[1]{\begin{itemize}[label=$\square$]\item #1\end{itemize}}
\newcommand{\hdone}[1]{\begin{itemize}[label=$\boxtimes$]\item #1\end{itemize}}
\newcommand{\hdiscard}[1]{\begin{itemize}[label=$\boxminus$]\item #1\end{itemize}}
\newcommand{\hoptionoff}[1]{\begin{itemize}[label=$\circ$]\item #1\end{itemize}}
\newcommand{\hoptionon}[1]{\begin{itemize}[label=$\bullet$]\item #1\end{itemize}}

\newcommand{\hfig}[3]{
	\begin{figure}[htb]
		\centering
		\begin{minipage}{0.9\textwidth}
			\begin{tcolorbox}[colback=lightgray, colframe=white]
				\begin{center}
					\includegraphics[width=#1\textwidth]{#2}
				\end{center}
				\captionof{figure}{\label{fig:#2}\emph{#3}}
			\end{tcolorbox}
		\end{minipage}
	\end{figure}
}
\newcommand{\hfigref}[1]{\emph{Fig.\,\ref{fig:#1}}.\/}

% This command needs to be used when the same figure is used more than once
% in the text as normally the name of the image file is used as a label, but
% in these cases it would lead to label duplication.
\newcommand{\hfigwithref}[4]{
	\begin{figure}[htb]
		\centering
		\begin{minipage}{0.9\textwidth}
			\begin{tcolorbox}[colback=lightgray, colframe=white]
				\begin{center}
					\includegraphics[width=#1\textwidth]{#2}
				\end{center}
				\captionof{figure}{\label{fig:#3}\emph{#4}}
			\end{tcolorbox}
		\end{minipage}
	\end{figure}
}

% Custom counter is needed for the tables because of a limitation
% of the longtable package (built-in counters jump two).
\newcounter{htablecnt}
\setcounter{htablecnt}{0}
\newcommand{\rtable}[1]{\refstepcounter{htablecnt}\label{tbl:#1}}

\newenvironment{htbl}[2]{
\begin{center}
\begin{tcolorbox}[colback=lightgray, colframe=white, width=0.9\textwidth, breakable]
	\rtable{#2}
	\begin{center}
		\textcolor{h119blue}{\textbf{{Table~\arabic{htablecnt}:}}~~\textit{#1}}
	\end{center}
}
{
\end{tcolorbox}
\end{center}
}
\newcommand{\htblref}[1]{\emph{Table\,\ref{tbl:#1}}.\/}

% enumitem settings:
% 	* the leftmargins are set to start the itemization and enumeration with a large indentation
%	* for presentation styles these values should be around 0.7cm
\setlist{noitemsep}
\setlist[itemize,1]{label=\raisebox{0.3ex}{\tiny$\blacksquare$}, leftmargin=1.5cm}
\setlist[itemize,2]{label=\scriptsize$\blacktriangleright$}
\setlist[enumerate,1]{leftmargin=1.5cm}
% enumitem settings -- END

\newcounter{hversion}
\setcounter{hversion}{0}
\newcommand{\addhversion}{\stepcounter{hversion}\arabic{hversion}}

\newenvironment{hversioninfo}{
	\newpage
	\null
	\vfill
	\sectiontop{Version info}\label{sec:version-info}
	\begin{minipage}{0.9\textwidth}
	\begin{mdframed}[backgroundcolor=lightgray, linewidth=0pt]
	\renewcommand{\arraystretch}{1.2}
	\setlength{\tabcolsep}{1em}
	\vspace{-2.2ex}
	\begin{longtable}{ccp{7cm}}
	\multicolumn{3}{l}{\textcolor{h119blue}{\textbf{{\normalsize$\blacksquare$}~~\textsc{Version Info}}}} \\
	\arrayrulecolor{h119blue}\hline
	\textcolor{h119blue}{\textbf{Version}} & \textcolor{h119blue}{\textbf{Date}} &
	\textcolor{h119blue}{\textbf{Modification}} \\
}
{
	\end{longtable}
	\end{mdframed}
	\end{minipage}
	\addcontentsline{toc}{section}{Version info}
}

\newcommand{\hversionitem}[4]{
	\textcolor{h119blue}{\textit{\addhversion}} & \textcolor{h119blue}{\textit{\formatdate{#1}{#2}{#3}}} & \textcolor{h119blue}{\textit{#4}} \\
}

% Enviroment with frame
%\newmdenv[topline=false,bottomline=false,rightline=false,leftline=false]{hinterfaceframe}
%
%\newcommand{\hinterfacehead}[5]{
%	\vspace{2ex}
%	\begin{hinterfaceframe}
%	{\small\textcolor{h119blue}{\textsc{\textbf{Interface}}}}\\
%	\footnotesize
%
%	\noindent\textbf{Bus type:}~\texttt{#1}
%
%	\vspace{0.5ex}
%	\noindent\textbf{D-BUS Name:}~\texttt{#2}
%	
%	\vspace{0.5ex}
%	\noindent\textbf{Object Path:}~\texttt{#3}
%
%	\vspace{0.5ex}
%	\noindent\textbf{Interface Name:}~\texttt{#4}
%	
%	\vspace{0.5ex}
%	\noindent\textbf{Description:}~#5\\
%	\end{hinterfaceframe}
%}

%\newmdenv[topline=false,bottomline=false,leftline=false,rightline=false]{hapiframe}
%
%\newenvironment{hapisig}[1]
%{
%	\begin{adjustwidth}{2.2em}{2.2em}
%	\begin{hapiframe}
%	\footnotesize
%	%\setlength{\parskip}{0.8em}
%	\textcolor{h119blue}{$\blacksquare$~~\textbf{\texttt{#1}~(signal)}}
%	\vspace{0.5em}\\
%}
%{
%	\end{hapiframe}
%	\end{adjustwidth}
%	\vspace{1ex}
%}


